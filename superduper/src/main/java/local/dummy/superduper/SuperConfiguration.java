package local.dummy.superduper;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SuperConfiguration {
	@Bean
	public SuperDuperClass getSuperDuperClass() {
		return new SuperDuperClass();
	}
}
