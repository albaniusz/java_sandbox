package local.dummy.superduper;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ControlerController {

	private SuperDuperClass superDuperClass;

	public ControlerController(SuperDuperClass superDuperClass) {
		this.superDuperClass = superDuperClass;
	}

	@GetMapping("/super")
	public String getImportantData() {
		return superDuperClass.generateSuperImportantData();
	}
}
