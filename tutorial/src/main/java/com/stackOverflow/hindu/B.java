package com.stackOverflow.hindu;

public class B<T> {
    public void f() {
        System.out.println("I'm generic f()");

    }

    public void z() {
        System.out.println("I'm generic z()");
        f();
    }
}
