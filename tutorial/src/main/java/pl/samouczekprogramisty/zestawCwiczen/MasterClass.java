package pl.samouczekprogramisty.zestawCwiczen;

public class MasterClass {
	private String name;
	private int age;

	/**
	 * Napisz metodę, która zwróci Twój aktualny wiek.
	 *
	 * @return
	 */
	public int getAge() {
		return age;
	}

	/**
	 * Napisz metodę, która zwróci Twoje imię,
	 *
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * Napisz metodę, która jako argument przyjmuje 2 liczby i wypisuje ich sumę, różnicę i iloczyn,
	 *
	 * @param a
	 * @param b
	 */
	public void calculateTwoElements(int a, int b) {
		System.out.println(a + b);
		System.out.println(a - b);
		System.out.println(a * b);
	}

	/**
	 * Napisz metodę, która jako argument przyjmuje liczbę i zwraca true jeśli liczba jest parzysta,
	 *
	 * @param a
	 * @return
	 */
	public boolean isEven(int a) {
		return (a % 2 == 0) ? true : false;
	}

	/**
	 * Napisz metodę, która jako argument przyjmuje liczbę i zwraca true jeśli liczba jest podzielna przez 3 i przez 5,
	 *
	 * @param a
	 * @return
	 */
	public boolean isDividedBy3And5(int a) {
		return (a % 3 == 0 && a % 5 == 0) ? true : false;
	}

	/**
	 * Napisz metodę, która jako argument przyjmuje liczbę i zwraca go podniesionego do 3 potęgi,
	 *
	 * @param a
	 * @return
	 */
	public int calculateThirdPower(int a) {
		return a * a * a;
	}

	/**
	 * Napisz metodę, która jako argument przyjmuje liczbę i zwraca jej pierwiastek kwadratowy
	 * (poczytaj javadoc do klasy Math, znajdziesz tam metodę, która na pewno Ci pomoże),
	 *
	 * @param a
	 * @return
	 */
	public double calculateRoot(int a) {
		return Math.sqrt(a);
	}

	/**
	 * Napisz metodę, która jako argument przyjmie trzy liczby. Metoda powinna zwrócić true jeśli z odcinków
	 * o długości przekazanych w argumentach można zbudować trójkąt prostokątny.
	 *
	 * @param a
	 * @param b
	 * @param c
	 * @return
	 */
	public boolean calculateRectangularTriangle(int a, int b, int c) {
		return (a * a + b * b == c * c) ? true : false;
	}
}
