package pl.samouczekprogramisty.zestawCwiczen.pakiety;

/**
 * Utwórz klasę reprezentującą prostokąt, musi posiadać atrybuty długość i szerokość. Klasa powinna posiadać metody
 * obliczające pole, obwód i długość przekątnej.
 */
public class Rectangle {
	private int height;
	private int length;

	public int calculateArea() {
		return height * length;
	}

	public int calculatePerimeter() {
		return 2 * height + 2 * length;
	}

	public double calculateDiagonal() {
		return Math.sqrt(height * height + length * length);
	}
}
