package pl.samouczekprogramisty.zestawCwiczen.pakiety;

/**
 * Utwórz klasę o nazwie MyNumber, której jedyny konstruktor przyjmuje liczbę. Klasa powinna mieć następujące metody
 * MyNumber isOdd() – true jeśli atrybut jest nieparzysty,
 * MyNumber isEven() – true jeśli atrybut jest parzysty,
 * MyNumber sqrt() – pierwiastek z atrybutu,
 * MyNumber pow(MyNumber x) – atrybut podniesiony do potęgi x (przydatnej metody poszukaj w javadoc do klasy Math),
 * MyNumber add(MyNumber x) – zwraca sumę atrybutu i x opakowaną w klasę MyNumber,
 * MyNumber subtract(MyNumber x) – zwraca różnicę atrybutu i x opakowaną w klasę MyNumber.
 */
public class MyNumber {
	private int number;

	public MyNumber(int number) {
		this.number = number;
	}

	/**
	 * true jeśli atrybut jest nieparzysty,
	 *
	 * @return
	 */
	public boolean isOdd() {
		return number % 2 == 0 ? true : false;
	}

	/**
	 * true jeśli atrybut jest parzysty,
	 *
	 * @return
	 */
	public boolean isEven() {
		return number % 2 == 0 ? false : true;
	}

	/**
	 * pierwiastek z atrybutu,
	 *
	 * @return
	 */
	public double sqrt() {
		return Math.sqrt(number);
	}

	/**
	 * atrybut podniesiony do potęgi x (przydatnej metody poszukaj w javadoc do klasy Math),
	 *
	 * @param x
	 */
	public int pow(MyNumber x) {
		return (int) Math.pow(number, x.getNumber());
	}

	/**
	 * zwraca sumę atrybutu i x opakowaną w klasę MyNumber,
	 *
	 * @param x
	 */
	public MyNumber add(MyNumber x) {
		return new MyNumber(x.getNumber() + number);
	}

	/**
	 * zwraca różnicę atrybutu i x opakowaną w klasę MyNumber.
	 *
	 * @param x
	 */
	public MyNumber subtract(MyNumber x) {
		return new MyNumber(number - x.getNumber());
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}
}
