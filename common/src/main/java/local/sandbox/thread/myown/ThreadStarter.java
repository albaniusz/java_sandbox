package local.sandbox.thread.myown;

public class ThreadStarter {
    public static void main(String[] args) {
        Thread thread1 = new Thread(new RunnableItem());
        thread1.start();

        Thread thread2 = new Thread(new RunnableItem());
        thread2.start();
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }
}
