package local.sandbox.thread.myown;

public class RunnableItem implements Runnable {
    @Override
    public void run() {
        for (int x = 0; x < 9999; x++) {
            System.out.println(this + " " + x);
            try {
                Thread.sleep(150);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
