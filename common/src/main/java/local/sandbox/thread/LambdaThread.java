package local.sandbox.thread;

public class LambdaThread {
    public static void main(String[] args) {
        Thread lambdaThread = new Thread(() -> System.out.println("This is lambda thread"));
//        lambdaThread.run();
//        lambdaThread.start();

        lambdaThread.run();
        lambdaThread.run();
        lambdaThread.run();



    }
}
