package local.sandbox.thread;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Sort {
    public static void main(String[] args) {
        List<Person> people = Arrays.asList(
                new Person("Charles", "Dickens", 60),
                new Person("Lewis", "Carroll", 42),
                new Person("Thomas", "Carlyle", 51),
                new Person("Charlotte", "Bronte", 45),
                new Person("Matthew", "Arnold", 39)
        );

        // Step 1: Sort list by last name
        Collections.sort(people, (p1, p2) -> p1.getSurname().compareTo(p2.getSurname()));

        // Step 2: Create a method that prints all elements in the list
        System.out.println("Printing all persons");
        printAll(people);

        // Step 3: Create a method that prints all people that have last name beginning with C
        System.out.println("Print all persons that have last names begninng with C");
        printConditionally(people, p -> p.getSurname().startsWith("C"));

        System.out.println("Print all persons that have first names begninng with C");
        printConditionally(people, p -> p.getName().startsWith("C"));
    }

    private static void printConditionally(List<Person> people, Condition condition) {
        for (Person person : people) {
            if (condition.test(person)) {
                System.out.println(person);
            }
        }
    }

    private static void printAll(List<Person> people) {
        System.out.println(people);
    }
}

interface Condition {
    boolean test(Person p);
}

class Person {
    private String name;
    private String surname;
    private int age;

    public Person(String name, String surname, int age) {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", age=" + age +
                '}';
    }
}