package local.sandbox.thread;

public class Lorem {
    public static void main(String[] args) {
        LoremRunnable loremRunnable = new LoremRunnable();
        loremRunnable.go();
    }
}

class LoremRunnable {
    public void go() {
        Thread thread1 = new Thread(new Thread1());
        Thread thread2 = new Thread(new Thread2());

        thread1.start();
        thread2.start();
    }
}

class Thread1 implements Runnable {
    @Override
    public void run() {
        for (int x = 0; x < 10000; x++) {
            System.out.println("Thread1: ping");
            try {
                Thread.currentThread().sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

class Thread2 implements Runnable {
    @Override
    public void run() {
        for (int x = 0; x < 10000; x++) {
            System.out.println("Thread2: pong");
            try {
                Thread.currentThread().sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}