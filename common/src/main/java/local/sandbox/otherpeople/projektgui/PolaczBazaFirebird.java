package local.sandbox.otherpeople.projektgui;

import java.sql.*;

/**
 * PolaczBazaFirebird
 */
public class PolaczBazaFirebird {

	private static Connection connection;
	private static Statement statement;

	public static void main(String[] args) {

		try {
			Class.forName("org.firebirdsql.jdbc.FBDriver");
		} catch (ClassNotFoundException ex) {
			System.out.println("! Nie odnaleziono sterownika JDBC!");
		}

		// nawiazanie polaczenia
		try {
			connection = DriverManager.getConnection("jdbc:firebirdsql://localhost/C:/wypozycz.FDB", "SYSDBA", "masterkey");
			statement = connection.createStatement();
			System.out.println("Polaczono z baza danych!");
		} catch (SQLException ex) {
			System.out.println("! Wystapil blad podczas proby nawiazania polaczenia z baza danych!!!");
		}

		// wykonywanie zapytan SELECT do bazy
		try {
			ResultSet resultSet = statement.executeQuery("SELECT * FROM ksiazka;");
			ResultSetMetaData rsmd = resultSet.getMetaData();
			int liczbaKol = rsmd.getColumnCount();
			while (resultSet.next()) {
				String linia = new String();
				for (int i = 1; i <= liczbaKol; i++) {
					if (i > 1) linia = linia + " ";
					linia = linia + resultSet.getString(i);
				}
				System.out.println(linia);
			}
		} catch (SQLException ex) {
			System.out.println("! Wystapil blad podczas proby wykonania zapytania do bazy danych!!!");
		}

		// wykonywanie zapytan INSERT/DELETE/UPDATE do bazy
		try {
			int wynikUpdate = statement.executeUpdate("INSERT INTO CZYTELNIK VALUES (1,'Anna', 'Abacka', 'Rzeszow');");
			if (wynikUpdate > 0)
				System.out.println("Liczba zmodyfikowanych rekordow: " + wynikUpdate);
			else
				System.out.println("Nie zmodyfikowano zadnego rekordu");
		} catch (SQLException ex) {
			System.out.println("! Wystapil blad podczas proby aktualizacji bazy danych!!!");
		}

		// zamkniecie polaczenia
		try {
			statement.close();
			System.out.println("Pomyslnie zamknieto polaczenie z baza danych!");
		} catch (SQLException ex) {
			System.out.println("! Wystapil blad podczas proby zamkniecia polaczenia z baza danych!!!");
		}
	}
}
