package local.sandbox;

import java.util.HashSet;

public class SeatsGenerator {


    public static void main(String[] args) {
        int rows = 10;
        int seatsInRow = 4;
        int firstRowNumber = 2;

        HashSet<Seat> seats = new HashSet<>();

        for (int x = firstRowNumber; x < rows; x++) {
            for (char y = 'A'; y < (65 + seatsInRow); y++) {
                seats.add(new Seat("" + x + y));
            }
        }

        for (Seat seat : seats) {
            System.out.println(seat);
        }

//        return new HashSet<>();//TODO
    }
}


class Seat {
    private String seatNumber;

    public Seat(String seatNumber) {
        this.seatNumber = seatNumber;
    }

    @Override
    public String toString() {
        return seatNumber;
    }
}
