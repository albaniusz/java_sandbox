package local.sandbox.collections;

import it.unimi.dsi.fastutil.longs.LongArrayList;

public class SpecializedLong {
    public static void main(String[] args) {
        LongArrayList list = new LongArrayList();
        list.add(10L);

        if (list.contains(10L)) {
            System.out.println("1");
        } else {
            System.out.println("0");
        }
    }
}
