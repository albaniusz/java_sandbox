package local.sandbox.collections;

import java.util.HashSet;
import java.util.Set;

public class SetIteration {
	public static void main(String... args) {
		Set<String> sampleSet = new HashSet<>();
		sampleSet.add("Marcin");
		sampleSet.add("Adela");
		sampleSet.add("Marek");
		sampleSet.add("Magda");

		System.out.println("Iterowanie po zbiorze");

		for (int x = 0; x < 5; x++) {
			for (String item : sampleSet) {
				System.out.println(item);
			}
			System.out.println("----------------------------------------------------");
		}
	}
}
