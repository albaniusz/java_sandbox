package local.sandbox.java8.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

public class CompareDates {
	public static void main(String... args) throws ParseException {

		Date now = new Date();
		logger(now.toString());

		String date = "2019-08-21";
		String time = "12:25";

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date target = formatter.parse(date + " " + time);
		logger(target.toString());


		LocalDateTime localDateTime = target.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
		localDateTime = localDateTime.minusMinutes(15);
		Date targetMinus15min = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());

		logger(targetMinus15min.toString());


		if (now.after(targetMinus15min)) {
			System.out.println("NOW is after TAREGET, it is too late");
		}

	}

	static void logger(String message) {
		System.out.println(message);
	}
}
