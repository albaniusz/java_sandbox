package local.sandbox.java8.streams;

import java.util.Arrays;
import java.util.List;

public class Example1 {
	public static void main(String... args) {
		List fruits = Arrays.asList("apple", "banana", "cherry", "plum", "pear", "pinapple");

		fruits.stream()
				.filter(s -> s.toString().startsWith("p"))
				.map(s -> s.toString().toUpperCase())
				.sorted()
				.forEach(System.out::println);
	}
}
