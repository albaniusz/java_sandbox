package local.sandbox.java8.streams;

import java.util.Arrays;
import java.util.List;

public class IsStringInArray {
	public static void main(String[] args) {
		boolean flag = false;
		List<String> stringList = Arrays.asList("a", "b", "C", "XXX", "d");

		long counter = stringList.stream()
				.filter("XXX"::equals)
				.count();

		if (counter > 0) {
			flag = true;
		}
		
		System.out.println(flag);
	}
}
