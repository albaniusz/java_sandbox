package local.sandbox.java8.streams;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Filter {
	public static void main(String... args) {
		List<Beverage> beverages = Arrays.asList(
				new Beverage("Cola", 2),
				new Beverage("HipsterCola", 5),
				new Beverage("SuperHipsterCola", 5),
				new Beverage("UltraSuperHipsterCola", 10),
				new Beverage("CheapCola", 2)
		);

		List<Beverage> filtered = beverages.stream()
				.filter(b -> b.getName().startsWith("C"))
				.collect(Collectors.toList());
		System.out.println(filtered); // [Cola, CheapCola]

//		Map<Integer, List> colaByPrice = beverages.stream()
//				.collect(Collectors.groupingBy(b -> b.getPrice()));
//		colaByPrice.forEach((price, p) -> System.out.format("price %s: %s\n", price, p));
//		//price 2: [Cola, CheapCola] //price 5: [HipsterCola, SuperHipsterCola] //price 10: [UltraSuperHipsterCola]

		Double averagePrice = beverages.stream().
				collect(Collectors.averagingInt(b -> b.getPrice()));
		System.out.println(averagePrice); //4.8

		String specialOffer = beverages.stream()
				.filter(b -> b.getPrice() > 2)
				.map(b -> b.getName())
				.collect(Collectors.joining(" and ", "Special offer:", "are -20%."));
		// System.out.println(specialOffer); //Special Offer: HipsterCola and SuperHipsterCola and UltraSuperHipsterCola are -20%.
	}
}
