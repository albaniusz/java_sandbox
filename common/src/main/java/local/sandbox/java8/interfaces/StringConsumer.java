package local.sandbox.java8.interfaces;

@FunctionalInterface
public interface StringConsumer {
	void consumeString(String str);
}
