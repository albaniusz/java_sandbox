package local.sandbox.parking;

import java.util.Random;

/**
 * Car
 */
public class Car implements Runnable {
	private String name;
	private Parking parking;
	private boolean isParked = false;

	public Car(String name) {
		ParkingLogger.log("New Car with name %s", name);
		this.name = name;
	}

	@Override
	public void run() {
		ParkingLogger.log("%s started!", getName());

		for (int x = 0; x < 25; x++) {
			findPlaceToPark();
			if (isParked()) {
				break;
			}
		}

		if (isParked()) {
			ParkingLogger.log("%s finished", getName());
		} else {
			ParkingLogger.log("%s left parking!", getName());
		}
	}

	private void findPlaceToPark() {
		ParkingLogger.log("%s is trying to park", getName());

		Random random = new Random();

		for (ParkPlace parkPlace : parking.getParkPlaces()) {
			try {
				Thread.sleep(random.nextInt(100));
			} catch (InterruptedException e) {
				ParkingLogger.log(e.getMessage());
			}
			synchronized (Car.class) {
				if (parkPlace.isEmpty()) {
					try {
						parkPlace.parkHere(this);
						setParked(true);
						ParkingLogger.log("%s found parking place", getName());
						break;
					} catch (Exception e) {
						ParkingLogger.log(e.getMessage());
						ParkingLogger.log("%s tried park on occupied parking place!", getName());
					}
				}
			}
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Parking getParking() {
		return parking;
	}

	public void setParking(Parking parking) {
		this.parking = parking;
	}

	public boolean isParked() {
		return isParked;
	}

	public void setParked(boolean parked) {
		isParked = parked;
	}

	@Override
	public String toString() {
		return "Car{" +
				"name='" + name + '\'' +
				", parking=" + parking +
				", isParked=" + isParked +
				'}';
	}
}
