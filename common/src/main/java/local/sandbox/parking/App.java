package local.sandbox.parking;

/**
 * App
 */
public class App {
	public static void main(String... args) {
		ParkingLogger.log("App started");
		Parking parking = new Parking(10);

		for (int x = 0; x < 11; x++) {
			Car car = new Car("Car No: " + (x + 1));
			car.setParking(parking);
			new Thread(car).start();
		}

		ParkingLogger.log("App finished");
	}
}
