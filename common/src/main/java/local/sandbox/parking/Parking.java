package local.sandbox.parking;

import java.util.ArrayList;
import java.util.List;

/**
 * Parking
 */
public class Parking {
	private List<ParkPlace> parkPlaces = new ArrayList<>();

	public Parking(int pakingPlacesNumber) {
		ParkingLogger.log("New Parking with %d places", pakingPlacesNumber);
		for (int x = 0; x < pakingPlacesNumber; x++) {
			parkPlaces.add(new ParkPlace("Place No: " + (x + 1)));
		}
	}

	public List<ParkPlace> getParkPlaces() {
		return parkPlaces;
	}

	public void setParkPlaces(List<ParkPlace> parkPlaces) {
		this.parkPlaces = parkPlaces;
	}
}
