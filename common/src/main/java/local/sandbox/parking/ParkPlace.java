package local.sandbox.parking;

/**
 * ParkPlace
 */
public class ParkPlace {
	private String name;
	private Car parkedCar;

	public ParkPlace(String parkPlaceName) {
		ParkingLogger.log("New ParkingPlace with name %s", parkPlaceName);
		this.name = parkPlaceName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isEmpty() {
		return parkedCar == null ? true : false;
	}

	public void parkHere(Car car) throws Exception {
		ParkingLogger.log("%s parked on %s", car.getName(), getName());

		if (parkedCar!= null) {
			throw new Exception("Parking place is occupied!");
		}

		parkedCar = car;
	}

	@Override
	public String toString() {
		return "ParkPlace{" +
				"name='" + name + '\'' +
				", parkedCar=" + parkedCar +
				'}';
	}
}
