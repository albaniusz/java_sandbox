package local.sandbox.parking;

/**
 * ParkingLogger
 */
public class ParkingLogger {
	public static void log(String message) {
		System.out.println(message);
	}

	public static void log(String message, Object... params) {
		System.out.println(String.format(message, params));
	}
}
