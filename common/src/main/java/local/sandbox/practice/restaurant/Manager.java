package local.sandbox.practice.restaurant;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

public class Manager {
	private static Manager instance;
	private byte tableIndex = 0;
	private List<Table> tables = new ArrayList<>();
	private Queue<Order> orders = new LinkedBlockingQueue<>();
	private Queue<Dish> dishes = new LinkedBlockingQueue<>();

	private Manager() {
		for (int i = 0; i < 10; i++) {
			tables.add(new Table("Table No." + i));
		}
	}

	public static Manager getInstance() {
		if (instance == null) {
			synchronized (Manager.class) {
				if (instance == null) {
					instance = new Manager();
				}
			}
		}
		return instance;
	}

	public Queue<Order> getOrders() {
		return orders;
	}

	public void setOrders(Queue<Order> orders) {
		this.orders = orders;
	}

	public Queue<Dish> getDishes() {
		return dishes;
	}

	public void setDishes(Queue<Dish> dishes) {
		this.dishes = dishes;
	}
}
