package local.sandbox.practice.restaurant;

public class Waiter implements Runnable {
	private Manager manager = Manager.getInstance();
	public static boolean isRunning = false;

	@Override
	public void run() {
		while (!isRunning) {
			if (!manager.getDishes().isEmpty()) {

				Log.log("Waiter dish to table");
			}

			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
