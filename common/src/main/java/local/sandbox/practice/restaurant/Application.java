package local.sandbox.practice.restaurant;

public class Application {
	public static void main(String... args) {
		Thread waiter = new Thread(new Waiter());

		Thread cook = new Thread(new Cook());

		waiter.start();
		cook.start();


		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		Waiter.isRunning = false;
		Cook.isRunning = false;
	}
}
