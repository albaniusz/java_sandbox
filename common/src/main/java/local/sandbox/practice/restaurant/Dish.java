package local.sandbox.practice.restaurant;

public class Dish {
	private Table table;

	public Dish(Table table) {
		this.table = table;
	}

	public Table getTable() {
		return table;
	}

	public void setTable(Table table) {
		this.table = table;
	}
}
