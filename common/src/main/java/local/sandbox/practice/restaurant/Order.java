package local.sandbox.practice.restaurant;

public class Order {
	private Table table;

	public Order(Table table) {
		this.table = table;
	}

	public Table getTable() {
		return table;
	}

	public void setTable(Table table) {
		this.table = table;
	}
}
