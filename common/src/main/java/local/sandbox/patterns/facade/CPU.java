package local.sandbox.patterns.facade;

/**
 * CPU
 */
public class CPU {

	public void freeze() {
	}

	public void jump(long position) {
	}

	public void execute() {
	}
}
