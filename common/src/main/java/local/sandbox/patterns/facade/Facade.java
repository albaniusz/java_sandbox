package local.sandbox.patterns.facade;

/**
 * Facade
 */
public class Facade {

	public static void main(String[] args) {

		Computer facade = new Computer();
		facade.start();
	}
}
