package local.sandbox.patterns.facade;

/**
 * Computer
 */
public class Computer {

	public static final long BOOT_ADDRESS = 12345;
	public static final long BOOT_SECTOR = 32314;
	public static final int SECTOR_SIZE = 1213;

	private CPU processor;
	private Memory ram;
	private HardDrive hd;

	public Computer() {

		this.processor = new CPU();
		this.ram = new Memory();
		this.hd = new HardDrive();
	}

	public void start() {

		processor.freeze();
		ram.load(BOOT_ADDRESS, hd.read(BOOT_SECTOR, SECTOR_SIZE));
		processor.jump(BOOT_ADDRESS);
		processor.execute();
	}
}
