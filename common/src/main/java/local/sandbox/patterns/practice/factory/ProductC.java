package local.sandbox.patterns.practice.factory;

public class ProductC implements Product {
    @Override
    public String toString() {
        return ProductC.class.getCanonicalName();
    }
}
