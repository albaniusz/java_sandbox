package local.sandbox.patterns.practice.adapter;

public class Adapter implements InterfaceA {
    private InterfaceB interfaceBObject;

    public Adapter(InterfaceB interfaceBObject) {
        this.interfaceBObject = interfaceBObject;
    }

    @Override
    public String methodA() {
        return interfaceBObject.methodB().toString();
    }
}
