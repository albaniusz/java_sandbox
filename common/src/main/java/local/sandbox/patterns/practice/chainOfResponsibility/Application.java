package local.sandbox.patterns.practice.chainOfResponsibility;

public class Application {
    public static void main(String... args) {
        Application application = new Application();
        application.run();
    }

    public void run() {
        Step2 step2 = new Step2();
        Step1 step1 = new Step1(step2);
        step1.method(1, 2);
    }
}
