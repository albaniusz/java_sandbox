package local.sandbox.patterns.practice.adapter;

public class Application {
    public static void main(String... args) {
        InterfaceA adapter = new Adapter(new ClassB());

        Context context = new Context();
        context.useClass(adapter);
        context.doSomething();
    }
}
