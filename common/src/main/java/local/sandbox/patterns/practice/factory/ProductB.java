package local.sandbox.patterns.practice.factory;

public class ProductB implements Product {
    @Override
    public String toString() {
        return ProductB.class.getCanonicalName();
    }
}
