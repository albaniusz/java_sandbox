package local.sandbox.patterns.practice.factory;

public class Application {
    public static void main(String... args) {

        Product product = Factory.produce();
        System.out.println(product);

        Factory.setMode("B");
        Product product1 = Factory.produce();
        System.out.println(product1);
    }
}
