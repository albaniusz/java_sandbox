package local.sandbox.patterns.practice.chainOfResponsibility;

public class Step1 implements ChainElement {
    private ChainElement nextElement;

    public Step1(ChainElement nextElement) {
        this.nextElement = nextElement;
    }

    public void method(int a, int b) {
        if (a == b) {
            System.out.println("Done!");
        } else {
            nextElement.method(a, b);
        }
    }
}
