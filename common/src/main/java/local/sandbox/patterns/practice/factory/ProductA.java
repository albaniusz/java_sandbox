package local.sandbox.patterns.practice.factory;

public class ProductA implements Product {
    @Override
    public String toString() {
        return ProductA.class.getCanonicalName();
    }
}
