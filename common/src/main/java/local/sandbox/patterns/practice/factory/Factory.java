package local.sandbox.patterns.practice.factory;

public class Factory {
    private static String mode;

    static {
        mode = "A";
    }

    public static void setMode(String mode) {
        Factory.mode = mode;
    }

    public static Product produce() {
        switch (mode) {
            case "A":
                return new ProductA();
            case "B":
                return new ProductB();
            case "C":
                return new ProductC();
        }

        return null;
    }
}
