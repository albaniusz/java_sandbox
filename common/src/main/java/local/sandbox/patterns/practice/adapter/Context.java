package local.sandbox.patterns.practice.adapter;

public class Context {
    private InterfaceA object;

    public void useClass(InterfaceA object) {
        this.object = object;
    }

    public void doSomething() {
        System.out.println(object.methodA());
    }
}
