package local.sandbox.patterns.practice.chainOfResponsibility;

public class Step2 implements ChainElement {
    @Override
    public void method(int a, int b) {
        System.out.println(Step2.class.getName() + " Done!");
    }
}
