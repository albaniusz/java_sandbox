package local.sandbox.patterns.practice.adapter;

public class ClassA implements InterfaceA {
    @Override
    public String methodA() {
        return "Lorem Ipsum";
    }
}
