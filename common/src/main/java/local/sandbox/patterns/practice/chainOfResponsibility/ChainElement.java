package local.sandbox.patterns.practice.chainOfResponsibility;

public interface ChainElement {
    void method(int a, int b);
}
