package local.sandbox.patterns.practice.adapter;

public class ClassB implements InterfaceB {
    @Override
    public Integer methodB() {
        return 123;
    }
}
