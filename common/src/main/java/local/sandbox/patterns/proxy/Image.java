package local.sandbox.patterns.proxy;

/**
 * Image
 */
interface Image {
	public void displayImage();
}
