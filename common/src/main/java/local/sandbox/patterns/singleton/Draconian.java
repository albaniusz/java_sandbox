package local.sandbox.patterns.singleton;

public class Draconian {
    private static Draconian instance;

    private Draconian() {
    }

    public static synchronized Draconian getInstance() {
        if (instance == null) {
            instance = new Draconian();
        }
        return instance;
    }
}
