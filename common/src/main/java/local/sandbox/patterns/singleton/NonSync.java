package local.sandbox.patterns.singleton;

public class NonSync {
    private static NonSync instance;

    static {
        instance = new NonSync();
    }

    private NonSync() {
    }

    public static NonSync getInstance() {
        return instance;
    }
}
