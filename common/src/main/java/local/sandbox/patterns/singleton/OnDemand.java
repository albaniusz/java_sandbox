package local.sandbox.patterns.singleton;

public class OnDemand {
    private static class InstanceHolder {
        private static final OnDemand instance = new OnDemand();
    }

    public static OnDemand getInstance() {
        return InstanceHolder.instance;
    }
}
