package local.sandbox.patterns.singleton;

public class ThreadSafeLazyInitialization {
    private static class Loader {
        static final ThreadSafeLazyInitialization INSTANCE = new ThreadSafeLazyInitialization();
    }

    private ThreadSafeLazyInitialization() {
    }

    public static ThreadSafeLazyInitialization getInstance() {
        return Loader.INSTANCE;
    }
}
