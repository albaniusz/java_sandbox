package local.sandbox.patterns.flyweight;

/**
 * CoffeeOrder
 */
interface CoffeeOrder {
	public void serveCoffee(CoffeeOrderContext context);
}
