package local.sandbox.patterns.observer;

public interface Observer {
    public void update();
}
