package local.sandbox.patterns.observer;

public class Test {
    public static void main(String[] args) {
        SomeObservable someObservable= new SomeObservable();

        someObservable.attach(new SomeObserver());
        someObservable.attach(new SomeObserver());
        someObservable.attach(new SomeObserver());

        someObservable.notifyObservers();
    }
}
