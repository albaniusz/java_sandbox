package local.sandbox.patterns.observer;

public class SomeObserver implements Observer {
    @Override
    public void update() {
        System.out.println("Dong!");
    }
}
