package local.sandbox.patterns.factory;

/**
 * MagicMazeGame
 */
public class MagicMazeGame extends MazeGame {

	@Override
	protected Room makeRoom() {
		return new MagicRoom();
	}
}
