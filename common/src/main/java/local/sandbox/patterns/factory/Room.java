package local.sandbox.patterns.factory;

/**
 * Room
 */
public abstract class Room {

	private Room connection;

	public void connect(Room room) {
		connection = room;
	}
}
