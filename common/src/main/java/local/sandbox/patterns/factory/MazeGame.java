package local.sandbox.patterns.factory;

import java.util.ArrayList;
import java.util.List;

/**
 * MazeGame
 */
public class MazeGame {

	List<Room> rooms = new ArrayList<Room>();

	public MazeGame() {

		Room room1 = makeRoom();
		Room room2 = makeRoom();
		room1.connect(room2);
		this.addRoom(room1);
		this.addRoom(room2);
	}

	protected void addRoom(Room room) {
		rooms.add(room);
	}

	protected Room makeRoom() {
		return new OrdinaryRoom();
	}
}
