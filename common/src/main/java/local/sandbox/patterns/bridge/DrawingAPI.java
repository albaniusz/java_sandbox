package local.sandbox.patterns.bridge;

/**
 * DrawingAPI
 */
interface DrawingAPI {
	public void drawCircle(double x, double y, double radius);
}