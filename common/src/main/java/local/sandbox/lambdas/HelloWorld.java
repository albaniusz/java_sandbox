package local.sandbox.lambdas;

public class HelloWorld {
    public static void main(String[] args) {

        LoremLambda loremLambda = () -> System.out.println("Hello world!");
        printSomething(loremLambda);

    }

    public static void printSomething(LoremLambda ____) {
//        ____();
    }
}
