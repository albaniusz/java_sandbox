package local.sandbox.altkom.test;

import java.math.BigDecimal;

public class BigDecimalTest {
    public static void main(String... args) {
        BigDecimal one = BigDecimal.ONE;
        BigDecimal ten = BigDecimal.TEN;
        BigDecimal six = BigDecimal.valueOf(6);

        one.add(six).multiply(ten);
        System.out.println(one);
    }
}
