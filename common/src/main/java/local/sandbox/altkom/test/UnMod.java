package local.sandbox.altkom.test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class UnMod {
    private final List<String> data;

    public UnMod(List<String> data) {
        this.data = Collections.unmodifiableList(new ArrayList<>(data));
    }
}
