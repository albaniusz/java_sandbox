package local.sandbox.altkom.test;
class A {
    String getSomething() { return "A"; }
}

class B extends A {
    @Override
    String getSomething() { return "B"; }
}

public class HelloWorld {
    public static void main(String[] args) {

        A a = new A();
        A a1 = new B();
        A a2 = (A) new B();
        B b = new B();

        System.out.print(a.getSomething());
        System.out.print(a1.getSomething());
        System.out.print(a2.getSomething());
        System.out.print(b.getSomething());
    }
}

