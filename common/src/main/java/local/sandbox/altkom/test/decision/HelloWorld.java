package local.sandbox.altkom.test.decision;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class HelloWorld {

    public static void main(String[] args) {
        List<Decision> decisions = new ArrayList<>();

        decisions.add(new Decision(1L, LocalDate.of(2018, 6, 20), Decision.DecisionResult.ACCEPTED));
        decisions.add(new Decision(2L, LocalDate.of(2018, 5, 21), Decision.DecisionResult.DECLINED));
        decisions.add(new Decision(3L, LocalDate.of(2018, 1, 15), Decision.DecisionResult.DECLINED));
        decisions.add(new Decision(4L, LocalDate.of(2018, 5, 16), Decision.DecisionResult.ACCEPTED));
        decisions.add(new Decision(5L, LocalDate.of(2018, 2, 23), Decision.DecisionResult.ACCEPTED));

        String collect = decisions.stream()
                .filter(d -> Decision.DecisionResult.DECLINED.equals(d.getResult()))
                .map(Decision::getRequestId)
                .sorted(Long::compareTo)
                .map(Object::toString)
                .collect(Collectors.joining(","));

        System.out.println(collect);
    }
}

