package local.sandbox.altkom.test;


class Divider {

    Integer divide(int a, int b) {
        try {
            return a/b;
        } finally {
            System.out.println("Hello from finally block!");
        }
    }

}

public class HelloWorld2 {

    public static void main(String[] args) {
        try {
            Divider divider = new Divider();
            Integer result = divider.divide(10, 0);
            System.out.println(result);

        } catch(Exception ex) {
            System.out.println("Exception: Division by 0!");
        }
    }
}
