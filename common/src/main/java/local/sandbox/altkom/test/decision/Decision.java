package local.sandbox.altkom.test.decision;

import java.time.LocalDate;

class Decision {
    private Long requestId;
    private LocalDate date;
    private DecisionResult result;

    public Decision(Long requestId, LocalDate date, DecisionResult result) {
        this.requestId = requestId;
        this.date = date;
        this.result = result;
    }

    public Long getRequestId() {
        return requestId;
    }

    public void setRequest(Long requestId) {
        this.requestId = requestId;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public DecisionResult getResult() {
        return result;
    }

    public void setResult(DecisionResult result) {
        this.result = result;
    }

    public enum DecisionResult {
        ACCEPTED, DECLINED
    }
}
