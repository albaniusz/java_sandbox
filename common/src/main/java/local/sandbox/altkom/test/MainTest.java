package local.sandbox.altkom.test;

import java.util.Set;
import java.util.TreeSet;

public class MainTest {
    public static void main(String... args) {

        Set<Integer> numbers = new TreeSet<Integer>();

        numbers.add(3);
        numbers.add((int) 3.0);
        numbers.add(2);
        numbers.add(2);
        numbers.add(new Integer(2));
        numbers.add(Integer.parseInt("2"));

        System.out.println(numbers);
    }
}
