package local.sandbox.recurrent;

public class Error {
	public static void main(String... args) {
		Error error = new Error();
		error.method(error);
	}

	private void method(Error error) {
		error.method(error);
	}
}
