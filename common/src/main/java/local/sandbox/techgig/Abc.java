package local.sandbox.techgig;

import java.util.*;

public class Abc {
    public static void main(String[] args) {
        Map obj1 = new HashMap<String, Integer>();
        obj1.put("X", new Integer(3));
        obj1.put("Y", new Integer(2));
        obj1.put("Z", new Integer(8));
        obj1.remove(new String("X"));
        System.out.print(obj1);
    }
}
