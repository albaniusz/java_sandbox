package local.sandbox.socket;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class SocketTimeoutExceptionExmpl {
	public static void main(String[] args) {

		String host = "xxx";
		int port = 8080;

		Socket socket = new Socket();

		try {
			// Utworzenie adresów
			InetAddress inetadr = InetAddress.getByName(host);
			InetSocketAddress conadr = new InetSocketAddress(inetadr, port);

			// Połaczenie z serwerem
			// Określenie maksymalnego czasu oczekiwania na połączenie
			socket.connect(conadr, 200);

			// Pobranie strumienia wejściowego gniazda
			// Nakładamy buforowanie
			BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			// Okreslenie maksymalnego czasu oczekiwania na odczyt danych z serwera
			socket.setSoTimeout(50);

			// Odczyt odpowiedzi serwera (data i czas)
			String line;
			while ((line = br.readLine()) != null) {
				System.out.println(line);
			}

			// Zamknięcie strumienia i gniazda
			br.close();
			socket.close();
		} catch (UnknownHostException exc) {
			System.out.println("Nieznany host: " + host);
		} catch (Exception exc) {
			exc.printStackTrace();
		}
	}
}
