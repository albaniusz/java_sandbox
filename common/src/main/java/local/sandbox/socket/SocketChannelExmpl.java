package local.sandbox.socket;

import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.channels.SocketChannel;

public class SocketChannelExmpl {
    public static void main(String[] args) {

        SocketChannel channel;
        String server = ""; // adres hosta serwera
        int port = 123; // numer portu

        // ....

        try {
            channel = SocketChannel.open();
            channel.configureBlocking(false);
            channel.connect(new InetSocketAddress(server, port));

            System.out.print("Łącze się ...");
            while (!channel.finishConnect()) {
                // ew. pokazywanie czasu łączenia (np. pasek postępu)
                // lub wykonywanie jakichś innych (krótkotrwałych) działań
            }
        } catch (UnknownHostException exc) {
            System.err.println("Uknown host " + server);
            // ...
        } catch (Exception exc) {
            exc.printStackTrace();
            // ...
        }
        System.out.println("\nPołaczony");
    }
}
