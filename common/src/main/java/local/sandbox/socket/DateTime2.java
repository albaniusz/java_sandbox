package local.sandbox.socket;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class DateTime2 {
    public static void main(String[] args) {

        String host = "time.nist.gov";
        int port = 13;
        Socket socket = new Socket();

        try {
            // Utworzenie adresów
            InetAddress inetadr = InetAddress.getByName(host);
            InetSocketAddress conadr = new InetSocketAddress(inetadr, port);

            // Połaczenie z serwerem
            // Określenie maksymalnego czasu oczekiwania na połączenie
            socket.connect(conadr, 200);


            // Pobranie strumienia wejściowego gniazda
            // Nakładamy buforowanie
            BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            // Okreslenie maksymalnego czasu oczekiwania na odczyt danych z serwera
            socket.setSoTimeout(200);

            // Czego możemy się dowiedzieć o stanie gniazda?
            report(socket);

            // Odczyt odpowiedzi serwera (data i czas)
            String line;
            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }

            // Zamknięcie strumienia i gniazda
            br.close();
            socket.close();
        } catch (UnknownHostException exc) {
            System.out.println("Nieznany host: " + host);
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    // Dynamiczne wołanie metod z klasy Socket
    static void report(Socket s) throws Exception {
        Method[] methods = (java.net.Socket.class).getMethods();
        Object[] args = {};

        for (Method method : methods) {
            String name = method.getName();
            if ((name.startsWith("get") || name.startsWith("is")) &&
                !name.equals("getChannel") &&
                !name.equals("getInputStream") &&
                !name.equals("getOutputStream")) {

                System.out.println(name + "() = " +
                    method.invoke(s, args));
            }
        }
    }
}
