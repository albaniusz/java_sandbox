package local.sandbox.socket.dictGui;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class ReadDef extends Thread {

    private static final Charset charset = Charset.forName("ISO-8859-2");

    private static final ByteBuffer inBuf = ByteBuffer.allocateDirect(1024);

    private static final Matcher matchCode = Pattern.compile("(\n250 ok)|(552 no match)")
        .matcher("");

    private final SocketChannel channel;

    private final DictGui gui;

    private final String word;

    public ReadDef(DictGui gui, SocketChannel ch, String wordToSearch) {
        this.gui = gui;
        channel = ch;
        word = wordToSearch;
    }

    private StringBuffer result;

    public void run() {

        result = new StringBuffer("Wyniki wyszukiwania:\n");
        int count = 0, rcount = 0;
        try {
            CharBuffer cbuf = CharBuffer.wrap("DEFINE * " + word + "\n");
            ByteBuffer outBuf = charset.encode(cbuf);
            channel.write(outBuf);

            while (true) {
                inBuf.clear();
                int readBytes = channel.read(inBuf);
                if (readBytes == 0) {
                    gui.setInfo("Czekam ... " + ++count);
                    Thread.sleep(200);
                    continue;
                } else if (readBytes == -1) {
                    gui.setInfo("Kanał zamknięty");
                    channel.close();
                    break;
                } else {
                    inBuf.flip();
                    cbuf = charset.decode(inBuf);
                    result.append(cbuf);
                    matchCode.reset(cbuf);
                    if (matchCode.find()) break;
                    else gui.setInfo("Czytam ... " + ++rcount);
                }
            }
        } catch (Exception exc) {
            exc.printStackTrace();
            return;
        }
        gui.setInfo("Czekałem: " + count + " / Czytałem: " + rcount + ". Gotowe.");
    }

    public String getResult() {
        if (result == null) return "Brak wyników wyszukiwania";
        return result.toString();
    }
}
