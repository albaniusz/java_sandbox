package local.sandbox.socket.dictGui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.channels.SocketChannel;

public class DictGui extends JFrame implements ActionListener {

    public final static int port = 2628;

    private final String server;

    private SocketChannel channel;

    private final JTextArea ta = new JTextArea(20, 20);

    private final JTextField tf = new JTextField(20);

    private final JLabel infoLab = new JLabel("Nie było szukania");

    private final JButton paste = new JButton("Wklej definicję");

    private final Container cp = getContentPane();

    private ReadDef rd;

    public DictGui(String server) {

        this.server = server;

        // Otwarcie o połączenie kanału
        // metoda connect - zdefiniowana u końca klasy
        try {
            channel = SocketChannel.open();
            channel.configureBlocking(false);
            connect();
        } catch (UnknownHostException exc) {
            System.err.println("Uknown host " + server);
            System.exit(1);
        } catch (IOException exc) {
            exc.printStackTrace();
            System.exit(2);
        }

        // Konfiguracja GUI
        Font f = new Font("Dialog", Font.BOLD, 14);
        ta.setFont(f);
        tf.setFont(f);
        tf.setBorder(BorderFactory.createLineBorder(Color.orange, 1));
        infoLab.setPreferredSize(new Dimension(200, 30));
        JPanel p = new JPanel();
        p.setBorder(BorderFactory.createLineBorder(Color.red, 2));
        p.add(tf);
        p.add(infoLab);
        p.add(paste);
        cp.add(new JScrollPane(ta));
        cp.add(p, "South");

        tf.addActionListener(this);
        paste.addActionListener(this);

        // Przy zamykaniu aplikacji
        // zamykamy kanał i gniazdo
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                dispose();
                try {
                    channel.close();
                    channel.socket().close();
                } catch (Exception ignored) {
                }
                System.exit(0);
            }
        });

        pack();
        show();
    }

    // Obsługa akcji
    public void actionPerformed(ActionEvent e) {

        // Kliknięcie w przycisk "Wklej definicję"
        // definicję przechowuje dla nas obiekr klasy ReadDef
        if (e.getSource() == paste && rd != null) {
            ta.insert(rd.getResult(), ta.getCaretPosition());
        } else {  // ENTER na polu tekstowym tf - start wątku komuniakcji z serwerem
            if (!channel.isConnected()) try {
                connect();
            } catch (Exception exc) {
                exc.printStackTrace();
                return;
            }
            rd = new ReadDef(this, channel, tf.getText());
            rd.start();
        }
    }

    // Łączenie kanału z serwerem
    private void connect() throws UnknownHostException, IOException {

        if (!channel.isOpen())
            channel = SocketChannel.open();
        channel.connect(new InetSocketAddress(server, port));
        System.out.print("Łącze się ...");

        while (!channel.finishConnect()) {
            try {
                Thread.sleep(200);
            } catch (Exception exc) {
                return;
            }
            System.out.print(".");
        }

        System.out.println("\nPołączony.");
    }

    // Metoda wykorzystywana przez ReadDef
    // do pokazywania postepów komuniakcji z serwerem
    public void setInfo(String s) {
        infoLab.setText(s);
    }
}
