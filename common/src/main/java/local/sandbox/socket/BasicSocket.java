package local.sandbox.socket;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;

public class BasicSocket {
	public static void main(String[] args) {

		try {
			// Utworzenie gniazda
			String serverHost = "127.0.0.1"; // adres IP serwera ("cyfrowo" lub z użyciem DNS)
			int serverPort = 8080;      // numer portu na którym nasłuchuje serwer

			Socket socket = new Socket(serverHost, serverPort);

			// Uzyskanie strumieni do komunikacji
			OutputStream sockOut = socket.getOutputStream();
			InputStream sockIn = socket.getInputStream();

			// Komunikacja (zależna od protokołu)

			// Wysłanie zlecenia do serwera
			sockOut.write("xxx".getBytes());
			// ...

			// Odczytanie odpowiedzi serwera
			sockIn.read("yyy".getBytes());
			// ...

			// Po zakończeniu komunikacji - zamkniecie strumieni i gniazda
			sockOut.close();
			sockIn.close();
			socket.close();

		} catch (UnknownHostException exc) {
			// nieznany host
		} catch (SocketException exc) {
			// wyjątki związane z komunikacją przez gniazda
		} catch (IOException exc) {
			// inne wyjątki we/wy
		}
	}
}
