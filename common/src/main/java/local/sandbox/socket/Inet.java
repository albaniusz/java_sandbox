package local.sandbox.socket;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

public class Inet {
	public static void main(String[] args) {

		String host = "time.nist.gov";
		int port = 13;

		try {
			// Utworzenie adresu
			InetAddress inetadr = InetAddress.getByName(host);

			// Utworzenie gniazda
			Socket socket = new Socket(inetadr, port);

			// ....

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
