package local.sandbox.spox;

import java.util.Scanner;

/**
 * Wprowadzenie
 */
class Wprowadzenie {

	public static void main(String[] args) throws Exception {

		int n, d;

		Scanner myScanner = new Scanner(System.in);
		int currentNumber;
		n = myScanner.nextInt();
		d = myScanner.nextInt();

		int inputCounter = 0;
		int sum = 0;
		while (inputCounter <= d) {
			sum += myScanner.nextInt();
			inputCounter++;
		}

		int counter = 0;
		int buffer = sum;
		boolean isRest = true;
		while (isRest) {
			counter++;
			buffer = buffer / d;
			if (buffer == 0) {
				isRest = false;
			}
		}

		System.out.println(counter);
	}
}
