package local.sandbox.iLoveJava;

import java.io.Closeable;
import java.io.IOException;

public class ExceptionChallenge2 {
	public static void main(String... args) {
		String soprano = null;

		CloseIt closeIt = new CloseIt();

//		try (closeIt) {
		try {
			System.out.println(soprano.matches(null));
		} catch (RuntimeException e) {
//			try (closeIt) {
			try {
				System.out.println("runtime");
				throw new StackOverflowError();
			} catch (Exception r) {
				System.out.println("exception");
			}
		} catch (Error e) {
			System.out.println("error");
		} catch (Throwable throwable) {
			System.out.println("thowable");
		}
	}

	static class CloseIt implements Closeable {
		@Override
		public void close() throws IOException {
			System.out.println("close");
		}
	}
}
