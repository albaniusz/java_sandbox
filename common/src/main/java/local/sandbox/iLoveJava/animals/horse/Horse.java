package local.sandbox.iLoveJava.animals.horse;

import local.sandbox.iLoveJava.animals.animal.Animal;

public class Horse extends Animal {
	@Override
	protected boolean canFly() {
		return false;
	}

	public static void main(String... args) {
		System.out.println(new Horse().canFly());
	}
}
