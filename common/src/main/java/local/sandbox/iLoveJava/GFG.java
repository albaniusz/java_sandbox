package local.sandbox.iLoveJava;

public class GFG {
    static int countAllPairs(int n, int k) {
        int count = 0;

        if (n > k) {
            count = n - k;
            for (int i = k + 1; i <= n; i++) {
                count = count + ((n - k) / i);
            }
        }

        return count;
    }

    public static void main(String[] args) {
        int N = 11, K = 5;
        System.out.println(countAllPairs(N, K));
    }
}
