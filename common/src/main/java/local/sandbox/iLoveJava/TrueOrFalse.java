package local.sandbox.iLoveJava;

public class TrueOrFalse {
    public static void main(String... args) {
        boolean b1 = Boolean.valueOf("1");
        boolean b2 = Boolean.valueOf("0");
        System.out.println(b1 || b2);
    }
}
