package local.sandbox.iLoveJava;

public class MyThread {
	public static void main(String... args) {
		Thread t = new Thread(() -> System.out.println("Child thread"));
		t.start();

		System.out.println("Main Thread");
	}
}
