package local.sandbox.iLoveJava;

import java.util.*;

public class GFGFlattenList {
	public static <T> List<T> flattenStream(Collection<List<T>> lists) {
		List<T> finalList = new ArrayList<>();

		for (List<T> list : lists) {
			list.stream()
					.forEach(finalList::add);
		}

		return finalList;
	}

	public static void main(String... args) {
		Map<Integer, List<Character>> map = new HashMap<>();
		map.put(1, Arrays.asList('G', 'e', 'e', 'k', 's'));
		map.put(2, Arrays.asList('F', 'o', 'r'));
		map.put(3, Arrays.asList('G', 'e', 'e', 'k', 's'));

		List<Character> flatList = flattenStream(map.values());

		System.out.println(flatList);
	}
}
