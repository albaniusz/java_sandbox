package local.sandbox.iLoveJava;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JavaRegEx3 {
    public static void main(String[] args) {
        List<String> numbers = Arrays.asList("1", "20", "A1", "333", "A2A211");
        Pattern pattern = Pattern.compile("\\d+");

        for (String number : numbers) {
            Matcher matcher = pattern.matcher(number);
            while (matcher.find()) {
                System.out.println(matcher.group(0));
            }
        }
    }
}
