package local.sandbox.iLoveJava;

import java.util.Arrays;
import java.util.List;

public class JavaRegEx2 {
    public static void main(String[] args) {
        List<String> numbers = Arrays.asList("1", "20", "A1", "333", "A2A2111");

        for (String number : numbers) {
            System.out.println(number.replaceAll("\\d+", "#"));
        }

        numbers.stream()
                .map(x -> x.replaceAll("\\d+", "#"))
                .forEach(System.out::println);
    }
}
