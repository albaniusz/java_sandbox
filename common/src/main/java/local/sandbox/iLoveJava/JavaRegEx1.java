package local.sandbox.iLoveJava;

import java.util.Arrays;
import java.util.List;

public class JavaRegEx1 {
    public static void main(String[] args) {
        List<String> numbers = Arrays.asList("1", "20", "A1", "333", "A2A2111");

        for (String number : numbers) {
            if (number.matches("\\d+")) {
                System.out.println(number);
            }
        }

        numbers.stream()
                .filter(x -> x.matches("\\d+"))
                .forEach(System.out::println);
    }
}
