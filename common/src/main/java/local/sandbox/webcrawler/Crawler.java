package local.sandbox.webcrawler;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashSet;

public class Crawler {
	private HashSet<String> links;
	FileOutputStream outputStream;
	private int counter;

	public Crawler(String pathToFIle) throws FileNotFoundException {
		links = new HashSet<>();
		outputStream = new FileOutputStream(pathToFIle);

	}

	private void writeToFile(String line) {
		line += "\n";
		byte[] strToBytes = line.getBytes();
		try {
			outputStream.write(strToBytes);
			outputStream.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void close() {
		try {
			outputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws IOException {
		Crawler crawler = new Crawler("resources/filmypl.out");
		crawler.getPageLinks("http://www.filmy.pl/");
		crawler.close();
	}

	public void getPageLinks(String URL) {
		if (!links.contains(URL)) {
			try {

				if (links.add(URL)) {
					System.out.println(URL);
					writeToFile(URL);
					counter++;
				}

				if (counter > 10) {
//					return;
				}

				Document document = Jsoup.connect(URL).get();
				Elements linksOnPage = document.select("a[href]");

				for (Element page : linksOnPage) {
					String href = page.attr("abs:href");
					if (!href.contains("filmy.pl")
							|| href.contains("forum")
							|| href.contains("uzytkownik")
							|| href.contains("sklep/stereo")
							|| href.contains("%")
					) {
						continue;
					}
					getPageLinks(href);
				}
			} catch (IllegalArgumentException e) {
				System.err.println("For '" + URL + "': " + e.getMessage());
			} catch (IOException e) {
				System.err.println("For '" + URL + "': " + e.getMessage());
			}
		}
	}
}
