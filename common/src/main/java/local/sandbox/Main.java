package local.sandbox;

import java.util.function.BiConsumer;

public class Main {
    public static void main(String[] args) {


        BiConsumer<Integer, Long> multiplier = (Integer x, Long y) -> System.out.println(x * y);

        multiplier.accept(4, 2L);
    }
}
