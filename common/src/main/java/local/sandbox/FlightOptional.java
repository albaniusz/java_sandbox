package local.sandbox;

import java.util.Optional;
import java.util.Set;

public class FlightOptional {
    public static void main(String[] args) {
        FlightManager flightManager = new FlightManager();

        flightManager.findFlight("AAA123");

    }
}

class Flight {
    private String flightNumber;
    private String from;
    private String to;

    public Flight(String flightNumber, String from, String to) {
        this.flightNumber = flightNumber;
        this.from = from;
        this.to = to;
    }

    public String getFlightNo() {
        return flightNumber;
    }
}

class FlightManager {
    private Set<Flight> flights;

    public Optional<Flight> findFlight(String flightNo) {


        flights.add(new Flight(flightNo, "", ""));


        for (Flight flight : flights) {
            if (flight.getFlightNo().equals(flightNo)) {
                return Optional.of(flight);//FIXME
            }
        }


        return Optional.empty();//TODO
    }
}

