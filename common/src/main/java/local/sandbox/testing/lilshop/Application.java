package local.sandbox.testing.lilshop;

public class Application {
	public static void main(String... args) {
		Basket basket = new Basket();


		Item item1 = new Item("Item 1", 9.99);
		Item item2 = new Item("Item 2", 12.99);
		Item item3 = new Item("Item 3", 4.99);
		Item item4 = new Item("Item 4", 8.99);
		Item item5 = new Item("Item 5", 11.99);


		basket.add(item1);
		basket.add(item1);
		basket.add(item2);

		basket.add(item3, item4, item5);

		basket.remove(item3);

		System.out.println(basket.calculateAmount());
		basket.display();
	}
}
