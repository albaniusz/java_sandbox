package local.sandbox.testing.lilshop;

import java.util.HashMap;
import java.util.Map;

public class Basket {
	private Map<Item, Short> orderedItems = new HashMap<>();

	public void add(Item... items) {
		for (Item item : items) {
			if (orderedItems.containsKey(item)) {
				Short counter = orderedItems.get(item);
				orderedItems.put(item, ++counter);
			} else {
				orderedItems.put(item, (short) 1);
			}
		}
	}

	public void remove(Item... items) {
		for (Item item : items) {
			if (orderedItems.containsKey(item)) {
				orderedItems.remove(item);
			}
		}
	}

	public double calculateAmount() {
		double result = 0.0;

		for (Map.Entry<Item, Short> element : orderedItems.entrySet()) {
			result += element.getKey().getPrice() * element.getValue();
		}

		return result;
	}

	public void display() {
		orderedItems.forEach((k, v) -> System.out.println("Item: " + k.getName() + " (" + k.getPrice() + "), Count: "
				+ v + " Price: " + (v * k.getPrice())));
	}
}
