package local.sandbox.testing.lilshop;

import java.util.Objects;

public class Item {
	private double price;
	private String name;

	public Item(String name, double price) {
		this.name = name;
		this.price = price;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Item{" +
				"price=" + price +
				", name='" + name + '\'' +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Item)) return false;
		Item item = (Item) o;
		return Double.compare(item.getPrice(), getPrice()) == 0 &&
				Objects.equals(getName(), item.getName());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getPrice(), getName());
	}
}
