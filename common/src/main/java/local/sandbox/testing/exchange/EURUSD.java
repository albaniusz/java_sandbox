package local.sandbox.testing.exchange;

public class EURUSD extends CurrencyAbstract {
	public EURUSD() {
		baseCurrency = Currency.EUR;
		targetCurrency = Currency.USD;
//		reference = 1.131151;
		reference = 0.884055268;
//		spread = 0.05;
		spread = 0;
	}
}
