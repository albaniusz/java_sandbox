package local.sandbox.testing.exchange;

public class PLNEUR extends CurrencyAbstract {
	public PLNEUR() {
		baseCurrency = Currency.PLN;
		targetCurrency = Currency.EUR;
		reference = 4.3148;
//		reference = 0.228983575;
//		spread = 0.05;
		spread = 0;
	}
}
