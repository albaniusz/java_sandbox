package local.sandbox.testing.exchange;

public class Application {
	public static void main(String[] args) {
		double amount = 25D;

		PLNEUR plneur = new PLNEUR();
		plneur.convertFromBase(amount);
		plneur.convertToBase(amount);

		PLNUSD plnusd = new PLNUSD();
		plnusd.convertFromBase(amount);
		plnusd.convertToBase(amount);

		EURUSD eurusd = new EURUSD();
		eurusd.convertFromBase(amount);
		eurusd.convertToBase(amount);
	}
}
