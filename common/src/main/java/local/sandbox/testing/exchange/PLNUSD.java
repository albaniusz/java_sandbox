package local.sandbox.testing.exchange;

public class PLNUSD extends CurrencyAbstract {
	public PLNUSD() {
		baseCurrency = Currency.PLN;
		targetCurrency = Currency.USD;
		reference = 3.86078026;
//		reference = 0.259015;
		spread = 0.05;
//		spread = 0;
	}
}
