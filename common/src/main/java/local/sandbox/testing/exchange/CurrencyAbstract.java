package local.sandbox.testing.exchange;

public abstract class CurrencyAbstract {
	protected Currency baseCurrency;
	protected Currency targetCurrency;

	protected double reference = 1D;
	protected double spread = 0D;

	public double convertFromBase(double value) {
		double result = (value * reference) * (1D + spread);
		System.out.println(String.format("Convert %f%s to %f%s", value, targetCurrency, result, baseCurrency));
		return result;
	}

	public double convertToBase(double value) {
		double result = (value / reference) * (1D - spread);
		System.out.println(String.format("Convert %f%s to %f%s", value, baseCurrency, result, targetCurrency));
		return result;
	}
}
