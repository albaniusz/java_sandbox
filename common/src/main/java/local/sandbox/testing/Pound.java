package local.sandbox.testing;

import java.math.BigDecimal;

public class Pound {

	BigDecimal toKg = new BigDecimal(0.45359237);
	BigDecimal value;

	public Pound(BigDecimal value) {
		this.value = value;
	}

	public BigDecimal toKilograms() {
		return value.multiply(toKg);
	}
}
