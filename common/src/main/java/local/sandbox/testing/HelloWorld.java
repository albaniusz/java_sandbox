package local.sandbox.testing;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

class A {
	String getSomething() { return "A"; }
}

class B extends A {
	@Override
	String getSomething() { return "B"; }
}

public class HelloWorld {
	public static void main(String[] args) {

		BigDecimal one = BigDecimal.ONE;
		BigDecimal ten = BigDecimal.TEN;
		BigDecimal six = BigDecimal.valueOf(6);

		one.add(six).multiply(ten);

		System.out.println(one);
	}
}
