package local.sandbox.mockito.example;

public interface CustomerDao {
	boolean exists(int number);

	boolean save(Customer customer);
}
