package local.sandbox.mockito.helloapplication;

public interface Greeter {
	String getGreeting(String subject);

	String getIntroduction(String actor);
}
