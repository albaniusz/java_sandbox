package local.sandbox.mockito.helloapplication;

import java.io.IOException;

public interface HelloActable {
	void sayHello(String actor, String subject) throws IOException;
}
