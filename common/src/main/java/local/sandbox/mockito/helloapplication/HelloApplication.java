package local.sandbox.mockito.helloapplication;

import java.io.IOException;

public class HelloApplication {
	public static void main(String... args) throws IOException {
		new HelloAction(new HelloGreeter("hello", ": "), System.out).sayHello("application", "world");
	}
}
