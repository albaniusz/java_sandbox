package local.sandbox.mockito.helloapplication;

import java.io.IOException;

public class HelloAction implements HelloActable {
	private Greeter helloGreeter;
	private Appendable helloWriter;

	public HelloAction(Greeter helloGreeter, Appendable helloWriter) {
		super();
		this.helloGreeter = helloGreeter;
		this.helloWriter = helloWriter;
	}

	public void sayHello(String actor, String subject) throws IOException {
		helloWriter.append(helloGreeter.getIntroduction(actor)).append(helloGreeter.getGreeting(subject));
	}
}
