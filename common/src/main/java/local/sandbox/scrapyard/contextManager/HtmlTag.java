package local.sandbox.scrapyard.contextManager;

public class HtmlTag implements AutoCloseable {

    private String arg;

    public HtmlTag(String arg) {
        this.arg = arg;

        System.out.println("<" + this.arg + ">");
    }

    @Override
    public void close() {
        System.out.println("</" + arg + ">");
    }

    public void body(String arg) {
        System.out.println(arg);
    }
}
