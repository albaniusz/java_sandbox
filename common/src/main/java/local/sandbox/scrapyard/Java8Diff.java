package local.sandbox.scrapyard;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

public class Java8Diff {
	public static void main(String... args) {
		Button button = new Button("Click Me");
		button.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				System.out.println("Button clicked");
			}
		});

		Button button2 = new Button("Click Me");
		button2.setOnAction(event -> System.out.println("Button clicked"));
	}
}
