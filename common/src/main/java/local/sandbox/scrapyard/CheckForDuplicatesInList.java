package local.sandbox.scrapyard;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CheckForDuplicatesInList {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();

        list.add("aaa");
        list.add("bbb");
        list.add("ccc");
        list.add("ddd");
        list.add("ddd");
        list.add("eee");
        list.add("eee");
        list.add("fff");
        list.add("ggg");
        list.add("hhh");
        list.add("aaa");

        Set<String> set = new HashSet<>(list);

        String result;
        if (list.size() != set.size()) {
            result = "there is duplicates";
        } else {
            result = "there is no duplicates";
        }

        System.out.println(result);
    }
}
