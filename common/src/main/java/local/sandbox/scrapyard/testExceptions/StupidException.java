package local.sandbox.scrapyard.testExceptions;

public class StupidException extends Exception {
    public StupidException(String message) {
        super(message);
    }
}
