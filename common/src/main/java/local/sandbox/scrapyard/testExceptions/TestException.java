package local.sandbox.scrapyard.testExceptions;

public class TestException {
    public static void main(String[] args) throws Exception {
        try {
            System.out.println("try");

//            throw new StupidException("olaboga");
            throw new Exception("main");

//        } catch (Exception e) {
//            System.out.println("Exception e");
        } catch (StupidException e) {
            System.out.println("StupidException e");
        } finally {
            System.out.println("finally");
        }
    }
}
