package local.sandbox.scrapyard;

public class WhatIsThisCodeAbout {
    public static void main(String[] args) {
        int[] nums = {1, 3, 5, 7, 9};
        found:
        {
            for (int num : nums) {
                if (num == 5) {
                    break found;
                }
            }
            throw new AssertionError("5 not found");
        }

        System.out.println("All ok");
    }
}
