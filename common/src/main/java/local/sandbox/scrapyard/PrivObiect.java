package local.sandbox.scrapyard;

public class PrivObiect {
	private int x;

	public PrivObiect(int x) {
		this.x = x;
	}

	public static void main(String... args) {
		PrivObiect privObiect1 = new PrivObiect(1);
		PrivObiect privObiect2 = new PrivObiect(2);

		privObiect1.x = privObiect2.x;

		System.out.println(privObiect1.getX());
		System.out.println(privObiect2.getX());
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}
}
