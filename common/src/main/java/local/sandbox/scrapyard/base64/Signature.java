package local.sandbox.scrapyard.base64;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class Signature {
    public static void main(String[] args) throws NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException {


//        HMAC-SHA256(
//                base64urlEncoding(header) + '.' +
//                        base64urlEncoding(payload),
//                secret
//        )


        String secretKey = "ede0e488-d84f-4681-b4c9-711fcdee505d";
        String header = "{\"alg\": \"HS256\", \"typ\": \"JWT\"}";
        String payload = "{\"loggedInAs\": \"admin\", \"iat\" : 1422779638}";

        String toSHA256 = Base64.getUrlEncoder().encodeToString(header.getBytes()) + "." +
                Base64.getUrlEncoder().encodeToString(payload.getBytes());

        Mac mac = Mac.getInstance("HmacSHA256");
        SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey.getBytes(), "HmacSHA256");
        mac.init(secretKeySpec);
        String hmacSha256 = new String(mac.doFinal(toSHA256.getBytes()));



        System.out.println("x");
    }
}
