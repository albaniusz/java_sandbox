package local.sandbox.scrapyard;

import java.io.IOException;

public class Write {
    public static void main(String... args) throws IOException {
        Write write = new Write();
        String string = "loremipsum";
        write.write(string.toCharArray(), 3, 5);
    }

    public void write(char[] cbuf, int off, int len) throws IOException {
        for (int i = off; i < off + len; i++) {
            System.out.print(cbuf[i]);
        }
    }
}
