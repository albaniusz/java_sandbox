package local.sandbox.scrapyard;

import local.sandbox.tools.Logger;
import org.joda.time.DateTime;
import org.joda.time.Period;

/**
 * JodaTimeFull
 */
public class JodaTimeFull {
    /**
     * @param args
     */
    public static void main(String[] args) {

        Logger logger = new Logger();

        int periodInHours = Integer.parseInt("1");

        DateTime dateFrom = DateTime.now();
        logger.debug(dateFrom.toString());
        DateTime dateAfterPeriod = dateFrom.plus(Period.hours(periodInHours));
        logger.debug(dateAfterPeriod.toString());
    }
}
