package local.sandbox.scrapyard;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * RegEx
 *
 * @version $Id$
 */
public class RegEx {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		// P (402-P-01-00)
		// Z (402-Z-01-00)

		String line = "402-P-01-00  ";
		String pattern = "(\\d+)-([Pp]+)-(.*)";

		Pattern r = Pattern.compile(pattern);

		Matcher m = r.matcher(line);
		if (m.find()) {
			System.out.println("found");
		}
	}
}
