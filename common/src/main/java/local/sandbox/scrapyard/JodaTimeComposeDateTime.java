package local.sandbox.scrapyard;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class JodaTimeComposeDateTime {

    public static void main(String[] args) {


//        Tue Apr 22 05:34:00 CEST 2014

        String string = "2014-04-22 " + "2" + ":" + "05";

        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
//        formatter.print(localDate);

        formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm");
        DateTime dt = formatter.parseDateTime(string);



//        formatter.print(form.date.getValue());



        System.out.println(dt.toDateTime());
        System.out.println(dt.toDate());
    }
}
