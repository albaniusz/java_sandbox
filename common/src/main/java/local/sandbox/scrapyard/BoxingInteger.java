package local.sandbox.scrapyard;

public class BoxingInteger {
	public static void main(String[] args) {
		Integer x = 2;
		Integer y = new Integer(2);
		Integer z = Integer.valueOf(2);
		System.out.println(x == y);
		System.out.println(z == y);
		System.out.println(x == z);
	}
}
