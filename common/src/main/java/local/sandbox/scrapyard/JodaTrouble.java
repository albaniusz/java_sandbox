package local.sandbox.scrapyard;

import org.joda.time.DateTime;
import org.joda.time.Period;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class JodaTrouble {

    public static void main(String[] args) {

        Calendar now = GregorianCalendar.getInstance();
        int year = now.get(Calendar.YEAR);
        int month = now.get(Calendar.MONTH);
        int day = now.get(Calendar.DAY_OF_MONTH);
        int hour = now.get(Calendar.HOUR);
        int minute = now.get(Calendar.MINUTE);


//        hour = 13;

        year = (int) Math.round(year);

        int periodInHours = Integer.parseInt("2");

        DateTime dateFrom = new DateTime(year, month, day, hour, minute, 0, 0);
        DateTime dateAfterPeriod = dateFrom.plus(Period.hours(periodInHours));



        System.out.println(dateAfterPeriod.getHourOfDay());
        System.out.println(dateAfterPeriod.getMinuteOfHour());
        System.out.println(dateAfterPeriod.toDate());
    }
}
