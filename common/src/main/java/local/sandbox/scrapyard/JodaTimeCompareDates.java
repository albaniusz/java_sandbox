package local.sandbox.scrapyard;

import java.util.Date;

/**
 * JodaTimeCompareDates
 */
public class JodaTimeCompareDates {
    /**
     * @param args
     */
    public static void main(String[] args) {

        Date installmentPaymentLastDateAnd3 = new Date();
        Date positionSchedDate = new Date();

        org.joda.time.DateTime z1 = new org.joda.time.DateTime(installmentPaymentLastDateAnd3);
        org.joda.time.DateTime z2 = new org.joda.time.DateTime(positionSchedDate);

        if (z1.isAfter(z2)) {
            installmentPaymentLastDateAnd3 = positionSchedDate;
        }

        z1.plusYears(3);

        System.out.println(installmentPaymentLastDateAnd3);
    }
}
