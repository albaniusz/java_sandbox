package local.sandbox.scrapyard;

import java.util.*;

public class ByDictKey {

    public static void main(String[] args) {

        String result = "";

        Set<String> dictionary = new TreeSet<>();
        dictionary.add("a");
        dictionary.add("b");
        dictionary.add("c");
        dictionary.add("d");





        Map<String, List<String>> storage = new HashMap<>();
        for (String key : dictionary) {
            List<String> row = new ArrayList<>();
            storage.put(key, row);
        }

//        for (String paramsKey : params.keySet()) {
//            List<String> row = storage.get(paramsKey);
//            row.add(params.get(paramsKey));
//        }



        storage.get("c").add("c2");
        storage.get("b").add("b4");
        storage.get("a").add("a1");
        storage.get("b").add("b2");
        storage.get("a").add("a53");
        storage.get("a").add("a3");
        storage.get("b").add("b6");
        storage.get("d").add("d77");
        storage.get("b").add("b2");
        storage.get("d").add("d5");
        storage.get("b").add("b3");
        storage.get("c").add("c4");
        storage.get("c").add("c6");



        for (String key : dictionary) {
            List<String> row = storage.get(key);
            for (String element : row) {
                result += element;
            }
        }



        System.out.println("done! " + result);
    }
}
