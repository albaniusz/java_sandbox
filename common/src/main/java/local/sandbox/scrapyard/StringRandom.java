package local.sandbox.scrapyard;

import java.util.Random;

/**
 * StringRandom
 */
public class StringRandom {
	public static void main(String[] args) {

		char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
		StringBuilder sb = new StringBuilder();
		Random random = new Random();
		for (int i = 0; i < 20; i++) {
			char c = chars[random.nextInt(chars.length)];
			sb.append(c);
		}
		String output = sb.toString();
		System.out.println(output);
	}
}
