package local.sandbox.scrapyard.regex;

import java.util.regex.Pattern;

/**
 * IBANFormat
 */
public class IBANFormat {
    /**
     * @param args
     */
    public static void main(String[] args) {

        String ibanNumber = "25797430065339259446214923";
        String pattern = "(\\d{2})(\\d{4})(\\d{4})(\\d{4})(\\d{4})(\\d{4})(\\d{4})";
        String replacement = "$1 $2 $3 $4 $5 $6 $7";
        String result = Pattern.compile(pattern).matcher(ibanNumber).replaceAll(replacement);

        System.out.println(result);
    }
}
