package local.sandbox.scrapyard.regex;

/**
 * DeleteNonDigits
 */
public class DeleteNonDigits {

	public static void main(String[] args) {

		String processDefinitionId = "LoremIpsum-";
		Integer processDefId = Integer.valueOf(processDefinitionId.replaceAll("\\D", ""));

		System.out.println(processDefId);
	}
}
