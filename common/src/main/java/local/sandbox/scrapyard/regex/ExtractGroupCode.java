package local.sandbox.scrapyard.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * ExtractGroupCode
 */
public class ExtractGroupCode {

	/**
	 * @param args
	 */
	public static void main(String[] args) {


		String string = "281472#281472#U.0067: Usługi przeglądów i napraw kserokopiarek (plan-wykonanie: 2500.00; rezerwacje: 1219.51)";

		Pattern pattern = Pattern.compile("(\\d+)#(\\d+)#([.A-Z0-9]+):(.+)");
		Matcher matcher = pattern.matcher(string);
		if (matcher.find()) {
			System.out.println(matcher.group(3));
		}


		System.out.println("break!");
	}
}
