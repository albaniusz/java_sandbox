package local.sandbox.scrapyard;

import org.joda.time.DateTime;
import org.joda.time.Period;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class JodaTimeExp {

    /**
     * @param args
     */
    public static void main(String[] args) {

        Calendar now = GregorianCalendar.getInstance();
        int year = now.get(Calendar.YEAR);
        int month = now.get(Calendar.MONTH);
        int day = now.get(Calendar.DAY_OF_MONTH);
        int hour = now.get(Calendar.HOUR);
        int minute = now.get(Calendar.MINUTE);

        year = (int) Math.round(year);

        int periodInHours = (int) 24;

        DateTime dateFrom = new DateTime(year, month, day, hour, minute, 0, 0);
        DateTime dateAfterMonths = dateFrom.plus(Period.hours(periodInHours));

        System.out.println(dateFrom.toDate());
        System.out.println(dateAfterMonths.toDate());
        System.out.println(dateAfterMonths.toDateTime());


//        dateAfterMonths.toDate()


    }
}
