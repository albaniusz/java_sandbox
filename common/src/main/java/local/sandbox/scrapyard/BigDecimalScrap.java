package local.sandbox.scrapyard;

import java.math.BigDecimal;

/**
 * BigDecimalScrap
 */
public class BigDecimalScrap {

	public static final int MONEY_SCALE = 2;

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		BigDecimal mpkBrutto = new BigDecimal(200).setScale(MONEY_SCALE);
		BigDecimal mpkNetto = new BigDecimal(0).setScale(MONEY_SCALE);

		BigDecimal oneHundred = new BigDecimal(100).setScale(MONEY_SCALE);

		BigDecimal vatRate = new BigDecimal(23.00).setScale(MONEY_SCALE).add(oneHundred).divide(oneHundred, MONEY_SCALE);
		mpkNetto = mpkBrutto.divide(vatRate, MONEY_SCALE);




		System.out.println("sss");
	}
}
