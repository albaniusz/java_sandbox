package local.sandbox.scrapyard;

public class FileNameWithExtension {
	public static void main(String... args) {
		String fileName = "lorem.txt";
		System.out.print(fileName.substring(fileName.length() - 4));
		if (!".txt".equals(fileName.substring(fileName.length() - 4))) {
			System.out.print("It is not a txt file!");
		}
	}
}
