package local.sandbox.scrapyard;

import java.security.MessageDigest;

/**
 * MD5
 */
public class MD5 {

	public String generate(String string) {

		String output = "";

		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(string.getBytes());

			byte byteData[] = md.digest();

			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < byteData.length; i++) {
				sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
			}

			output = sb.toString();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return output;
	}
}
