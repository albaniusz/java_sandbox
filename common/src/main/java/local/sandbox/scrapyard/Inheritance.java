package local.sandbox.scrapyard;

public class Inheritance {
	public static void main(String... args) {
		B obj = new B();
		System.out.println(obj.calc());
	}
}

abstract class A {
	protected int a = 1;
	protected int b = 1;

	public int calc() {
		return a + b;
	}
}

class B extends A {
	protected int a = 2;
	protected int b = 2;
}
