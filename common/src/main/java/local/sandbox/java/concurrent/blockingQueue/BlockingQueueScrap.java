package local.sandbox.java.concurrent.blockingQueue;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * BlockingQueueScrap
 */
public class BlockingQueueScrap {

	public static void main(String[] args) throws InterruptedException {

		BlockingQueue<String> queue = new ArrayBlockingQueue<String>(1024);

		queue.put("1");

		String string = queue.take();
		System.out.println(string);
	}
}
