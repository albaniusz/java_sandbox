package local.sandbox.java.concurrent.countDownLatch;

import java.util.concurrent.CountDownLatch;

/**
 * Waiter
 */
public class Waiter implements Runnable {

	CountDownLatch latch = null;

	public Waiter(CountDownLatch latch) {
		this.latch = latch;
	}

	public void run() {

		try {
			latch.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		System.out.println("Waiter Released");
	}
}
