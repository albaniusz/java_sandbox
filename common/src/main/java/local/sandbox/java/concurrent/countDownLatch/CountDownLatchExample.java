package local.sandbox.java.concurrent.countDownLatch;

import java.util.concurrent.CountDownLatch;

/**
 * CountDownLatchExample
 */
public class CountDownLatchExample {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws InterruptedException {

		CountDownLatch latch = new CountDownLatch(1);

		Waiter waiter = new Waiter(latch);
		Decrementer decrementer = new Decrementer(latch);

		new Thread(waiter).start();
		new Thread(decrementer).start();

		Thread.sleep(4000);
	}
}
