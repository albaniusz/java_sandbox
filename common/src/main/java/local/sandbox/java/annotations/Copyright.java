package local.sandbox.java.annotations;

/**
 * Copyright
 *
 * Associates a copyright notice with the annotated API element.
 */
public @interface Copyright {
	String value();
}
