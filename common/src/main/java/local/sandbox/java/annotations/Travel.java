package local.sandbox.java.annotations;

import java.util.Date;

/**
 * Travel
 */
public class Travel {

	@RequestForEnhancement(
			id = 2868724,
			synopsis = "Enable time-travel",
			engineer = "Mr. Peabody",
			date = "4/1/3007"
	)
	public static void travelThroughTime(Date destination) {

	}
}
