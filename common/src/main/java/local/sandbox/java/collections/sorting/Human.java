package local.sandbox.java.collections.sorting;

/**
 * Human
 */
public class Human implements Comparable<Human> {

	private char sex;
	private String firstname;
	private String surname;

	public Human(char sex, String firstname, String surname) {
		this.sex = sex;
		this.firstname = firstname;
		this.surname = surname;
	}

	@Override
	public String toString() {
		return surname + " " + firstname + " (" + sex + ")";
	}

	public int compareTo(Human o) {

		int porownaneNazwiska = surname.compareTo(o.surname);

		if (porownaneNazwiska == 0) {
			return firstname.compareTo(o.firstname);
		} else {
			return porownaneNazwiska;
		}
	}

	public char getSex() {
		return sex;
	}

	public void setSex(char sex) {
		this.sex = sex;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}
}
