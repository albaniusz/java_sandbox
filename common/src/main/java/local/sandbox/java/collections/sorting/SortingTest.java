package local.sandbox.java.collections.sorting;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * SortingTest
 */
public class SortingTest {

	public static void main(String[] args) {
		SortingTest sort = new SortingTest();
		sort.sort();
	}

	public void sort() {
		List<Human> humans = new ArrayList<Human>();
		humans.add(new Human('K', "Asia", "Wczorajsza"));
		humans.add(new Human('M', "Marcin", "Nikczemny"));
		humans.add(new Human('M', "Slawek", "Abacki"));
		humans.add(new Human('K', "Kasia", "Borowik"));
		humans.add(new Human('K', "Dorota", "Borowik"));
		humans.add(new Human('M', "Tomek", "Daszek"));
		humans.add(new Human('K', "Ania", "Daszek"));

		for (Human human : humans) {
			System.out.println(human);
		}

		Collections.sort(humans, new KomparatorPlec());

		System.out.println("");

		for (Human human : humans) {
			System.out.println(human);
		}
	}

	private class KomparatorPlec implements Comparator<Human> {

		public int compare(Human o1, Human o2) {
			int sex = o1.getSex() - o2.getSex();
			if (sex == 0) {
				return o1.compareTo(o2);
			}

			return sex;
		}
	}
}
