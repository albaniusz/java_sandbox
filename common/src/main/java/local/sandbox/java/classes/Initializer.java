package local.sandbox.java.classes;

/**
 * Initializer
 */
public class Initializer extends PreInitializer {

	static {
		System.out.println("A");
	}

	{
		System.out.println("B");
	}

	public Initializer() {
		System.out.println("C");
	}

	public static void main(String[] args) {

		Initializer initializer = new Initializer();

	}
}
