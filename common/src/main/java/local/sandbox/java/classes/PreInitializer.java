package local.sandbox.java.classes;

/**
 * PreInitializer
 */
public class PreInitializer {
	static {
		System.out.println("PreA");
	}

	{
		System.out.println("PreB");
	}

	public PreInitializer() {
		System.out.println("PreC");
	}
}
