package local.sandbox.java.exception;

/**
 * Finally2
 */
public class Finally2 {

	public int returnValue(int x) {
		System.out.println("Returning value: " + x);
		return x;
	}

	public int testException() {

		try {
			throw new Exception();
		} catch (Exception e) {
			return returnValue(1);
		} finally {
			return returnValue(2);
		}
	}

	public static void main(String[] args) {
		System.out.println("Returned value: " + new Finally2().testException());
	}
}
