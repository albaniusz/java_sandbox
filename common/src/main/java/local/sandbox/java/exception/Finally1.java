package local.sandbox.java.exception;

/**
 * Finally1
 */
public class Finally1 {

	public int exceptionTest() {

		try {
			throw new Exception();
		} catch (Exception e) {
			return 1;
		} finally {
			return 2;
		}
	}

	public static void main(String[] args) {
		Finally1 finally1 = new Finally1();
		System.out.println(finally1.exceptionTest());
	}
}
