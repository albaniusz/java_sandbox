package local.sandbox.java.exception.parent;

/**
 * SuperException
 */
public class SuperException extends Exception {

	/**
	 * @param msg
	 */
	public SuperException(String msg) {
		super(msg);
	}
}
