package local.sandbox.java.exception.parent;

/**
 * SubSuperException
 */
public class SubSuperException extends SuperException {

	/**
	 * @param msg
	 */
	public SubSuperException(String msg) {
		super(msg);
	}
}
