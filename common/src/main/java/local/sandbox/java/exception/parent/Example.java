package local.sandbox.java.exception.parent;

/**
 * Example
 */
public class Example {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		try {
			System.out.println("try");

			if (true) {
//				throw new SuperException("rzucam wyjatek");
				throw new SubSuperException("rzucam wyjatek");
			}
		} catch (SuperException e) {
			System.out.println(e.getClass());

			String className = e.getClass().getName();

			if (className.equals("local.sandbox.java.exception.parent.SuperException")) {
				System.out.println("rzucam wyjatek");
			} else {
				System.out.println("loguje zdarzenie");
			}
		}
	}
}
