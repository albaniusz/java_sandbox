package local.sandbox.java.exception.parent;

/**
 * SubSubSuperException
 */
public class SubSubSuperException extends SubSuperException {

	/**
	 * @param msg
	 */
	public SubSubSuperException(String msg) {
		super(msg);
	}
}
