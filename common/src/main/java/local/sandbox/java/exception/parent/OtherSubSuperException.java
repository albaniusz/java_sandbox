package local.sandbox.java.exception.parent;

/**
 * OtherSubSuperException
 */
public class OtherSubSuperException extends SuperException {

	/**
	 * @param msg
	 */
	public OtherSubSuperException(String msg) {
		super(msg);
	}
}
