package local.sandbox.exercise.caesarCode;

/**
 * Main
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		char[] message = {'O', 'n', 'c', 'e', 'U', 'p', 'o', 'n', 'A', 'T', 'i', 'm', 'e'};
		char[] finalMessage = new char[message.length];

		int shift = 3;

		for (int i = 0; i < message.length; i++) {
			int asciiLetter = (int) message[i];
			finalMessage[i] = (char) (asciiLetter + shift);
		}

		System.out.println(message);
		System.out.println(finalMessage);
	}
}
