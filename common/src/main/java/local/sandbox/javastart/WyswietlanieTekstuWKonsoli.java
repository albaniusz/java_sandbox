package local.sandbox.javastart;

/**
 * Napisz program, który wyświetli w 3 kolejnych liniach trzy imiona: Ania, Bartek, Kasia.
 */
public class WyswietlanieTekstuWKonsoli {
	public static void main(String... args) {
		System.out.println("Ania\nBartek\nKasia");
	}
}
