package local.sandbox.javastart;

/**
 * Napisz program, w którym zadeklarujesz kilka zmiennych finalnych, lub zmiennych różnych typów o dowolnych nazwach,
 * a następnie wyświetlisz je w kolejnych wierszach.
 * <p>
 * W tym samym programie zadeklaruj cztery zmienne typu String. Trzy z nich zainicjuj jakimiś wyrazami,
 * a czwartemu przypisz ciąg znaków utworzony z połączenia trzech wcześniejszych zmiennych. Następnie wyświetl czwartą
 * zmienną na ekranie.
 * <p>
 * Przy użyciu metody substring wyświetl na ekranie dwa pierwsze wyrazy wykorzystując odwołując się wyłącznie
 * do czwartej zmiennej typu String.
 */
public class DefiniowanieZmiennych {
	public static void main(String... args) {

	}
}
