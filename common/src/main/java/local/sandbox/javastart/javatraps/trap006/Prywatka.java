package local.sandbox.javastart.javatraps.trap006;

public class Prywatka {
	private int x = 0;

	public Prywatka(int x) {
		this.x = x;
	}

	public boolean equal(Prywatka p) {
		return this.x == p.x;
	}

	public static void main(String[] args) {
		Prywatka a = new Prywatka(1);
		Prywatka b = new Prywatka(1);

		System.out.print(a.equal(b) + " ");
		System.out.print(a.x == b.x);
	}
}
