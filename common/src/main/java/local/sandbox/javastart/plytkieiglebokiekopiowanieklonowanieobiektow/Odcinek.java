package local.sandbox.javastart.plytkieiglebokiekopiowanieklonowanieobiektow;

public class Odcinek implements Cloneable {
	private Punkt start;
	private Punkt koniec;

	private Odcinek() {
	}

	public Odcinek(Punkt start, Punkt koniec) {
		this.start = start;
		this.koniec = koniec;
	}

	public Punkt getStart() {
		return start;
	}

	public void setStart(Punkt start) {
		this.start = start;
	}

	public Punkt getKoniec() {
		return koniec;
	}

	public void setKoniec(Punkt koniec) {
		this.koniec = koniec;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		Odcinek odcinek = new Odcinek();
		Punkt pStart = (Punkt) this.getStart().clone();
		Punkt pKoniec = (Punkt) this.getKoniec().clone();
		odcinek.setStart(pStart);
		odcinek.setKoniec(pKoniec);

		return odcinek;
	}
}
