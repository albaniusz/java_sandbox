package local.sandbox.javastart.plytkieiglebokiekopiowanieklonowanieobiektow;

public class Przyklad3 {
	public static void main(String[] args) throws CloneNotSupportedException {
		Punkt p1 = new Punkt(0, 0);
		Punkt p2 = new Punkt(10, 10);

		Odcinek odcinek1 = new Odcinek(p1, p2);
		Odcinek odcinek2 = (Odcinek) odcinek1.clone();

		boolean porownanie = odcinek1 == odcinek2;
		System.out.println("Porownanie: " + porownanie);
		System.out.println("Odcinek1 xStart: " + odcinek1.getStart().getX());
		System.out.println("Odcinek2 xStart: " + odcinek2.getStart().getX());

		odcinek2.getStart().setX(20);
		System.out.println("Odcinek1 xStart: " + odcinek1.getStart().getX());
		System.out.println("Odcinek2 xStart: " + odcinek2.getStart().getX());
	}
}
