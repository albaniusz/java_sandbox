package local.sandbox.javastart.plytkieiglebokiekopiowanieklonowanieobiektow;

public class Punkt implements Cloneable {
	private int x;
	private int y;

	public Punkt(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
