package local.sandbox.javastart.plytkieiglebokiekopiowanieklonowanieobiektow;

public class Przyklad2 {
	public static void main(String[] args) throws CloneNotSupportedException {
		Punkt punkt1 = new Punkt(10, 10);
		Punkt punkt2 = (Punkt) punkt1.clone();
		Punkt punkt3 = new Punkt(10, 10);
		boolean porownanie1 = punkt1 == punkt2;
		boolean porownanie2 = punkt1 == punkt3;

		System.out.println("Porownanie1: " + porownanie1);
		System.out.println("Porownanie2: " + porownanie2);
		wyswietl(punkt1, punkt2, punkt3);

		punkt2.setX(11);
		punkt3.setX(12);
		wyswietl(punkt1, punkt2, punkt3);
	}

	public static void wyswietl(Punkt... punkty) {
		int size = punkty.length;
		for (int i = 0; i < size; i++) {
			System.out.println("Punkt " + i + " x:" + punkty[i].getX() + " ; y:" + punkty[i].getY());
		}
		System.out.println();
	}
}
