package local.sandbox.hackerrank.challenges.javaendoffile;

import java.util.Scanner;

public class Solution {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int counter = 0;
		while (sc.hasNext()) {
			System.out.println(++counter + " " + sc.nextLine());
		}
		sc.close();
	}
}
