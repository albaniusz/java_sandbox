package local.sandbox.hackerrank.challenges.javaloopsi;

import java.util.Scanner;

public class Solution {
	private static final Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		int N = scanner.nextInt();
		scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
		scanner.close();

		for (int i = 1; i <= 10; i++) {
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append(N);
			stringBuilder.append(" x ");
			stringBuilder.append(i);
			stringBuilder.append(" = ");
			stringBuilder.append(N * i);
			System.out.println(stringBuilder.toString());
		}
	}
}
