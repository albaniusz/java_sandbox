package local.sandbox.hackerrank.challenges.javaloops;

import java.util.Scanner;

public class Solution {
	public static void main(String[] argh) {
		Scanner in = new Scanner(System.in);
		int t = in.nextInt();
		for (int i = 0; i < t; i++) {
			int a = in.nextInt();
			int b = in.nextInt();
			int n = in.nextInt();

			int sum = a;
			for (int s = 0; s < n; s++) {
				sum += ((int) Math.pow(2, s)) * b;
				System.out.print("" + sum + " ");
			}
			System.out.println();
		}
		in.close();
	}
}
