package local.sandbox.hackerrank.challenges.javaanagrams;

import java.util.Scanner;

public class Solution {

	static boolean isAnagram(String a, String b) {
		boolean isAnagram = true;

		a = a.toUpperCase();
		b = b.toUpperCase();

		int[] aArray = parseString(a);
		int[] bArray = parseString(b);

		for (int i = 0; i < aArray.length; i++) {
			if (aArray[i] != bArray[i]) {
				isAnagram = false;
				break;
			}
		}

		return isAnagram;
	}

	private static int[] parseString(String string) {
		int[] resultArray = new int[26];
		byte[] readBytes = string.getBytes();

		for (byte element : readBytes) {
			int index = element - 65;
			resultArray[index]++;
		}

		return resultArray;
	}

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		String a = scan.next();
		String b = scan.next();
		scan.close();
		boolean ret = isAnagram(a, b);
		System.out.println((ret) ? "Anagrams" : "Not Anagrams");
	}
}
