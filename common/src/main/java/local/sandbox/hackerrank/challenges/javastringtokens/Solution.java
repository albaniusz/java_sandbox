package local.sandbox.hackerrank.challenges.javastringtokens;

import java.util.Scanner;

public class Solution {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		String s = scan.nextLine();

		String[] array = s.trim().split("\\W+");
		System.out.println(array.length);

		for (String element : array) {
			System.out.println(element);
		}

		scan.close();
	}
}