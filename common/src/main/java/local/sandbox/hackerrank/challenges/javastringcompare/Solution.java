package local.sandbox.hackerrank.challenges.javastringcompare;

import java.util.Scanner;

public class Solution {
	public static String getSmallestAndLargest(String s, int k) {
		String smallest = "";
		String largest = "";

		// Complete the function
		// 'smallest' must be the lexicographically smallest substring of length 'k'
		// 'largest' must be the lexicographically largest substring of length 'k'

		int i = 0;
		while (i < s.length() - k + 1) {
			String part = s.substring(i, i + k);
			i++;

			if (smallest.isEmpty()) {
				smallest = part;
			} else if (part.compareTo(smallest) < 0) {
				smallest = part;
			}
			if (largest.isEmpty()) {
				largest = part;
			} else if (part.compareTo(largest) > 0) {
				largest = part;
			}
		}

		return smallest + "\n" + largest;
	}

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		String s = scan.next();
		int k = scan.nextInt();
		scan.close();

		System.out.println(getSmallestAndLargest(s, k));
	}
}
