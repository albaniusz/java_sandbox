package local.sandbox.hackerrank.challenges.javastringreverse;

public class Solution {
	public static void main(String[] args) {
//		Scanner sc = new Scanner(System.in);
//		String A = sc.next();
		/* Enter your code here. Print output to STDOUT. */


		String A = "madame";

		int a = 0;
		int b = A.length() - 1;
		boolean isPalindrome = false;
		while (true) {
			if (a == b || a > b) {
				isPalindrome = true;
				break;
			}

			if (A.charAt(a) != A.charAt(b)) {
				break;
			}

			a++;
			b--;
		}
		System.out.print(isPalindrome ? "Yes" : "No");
	}
}
