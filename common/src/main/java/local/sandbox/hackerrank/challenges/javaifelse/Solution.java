package local.sandbox.hackerrank.challenges.javaifelse;

import java.util.Scanner;

public class Solution {
	private static final Scanner scanner = new Scanner(System.in);

	public static void main(String... args) {
		int n = scanner.nextInt();
		scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
		scanner.close();

		String output;

		if (n % 2 != 0) { // If n is odd, print Weird
			output = "Weird";
		} else if (n >= 2 && n <= 5) { //If n is even and in the inclusive range of 2 to 5, print Not Weird
			output = "Not Weird";
		} else if (n >= 6 && n <= 20) { // If n is even and in the inclusive range of 6 to 20, print Weird
			output = "Weird";
		} else {
			output = "Not Weird";
		}

		System.out.println(output);
	}
}
