package local.sandbox.solution;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.Charset;

public class Solution3 {
    private static int resultCounter = 0;

    public static void main(String[] args) throws Exception {

        String s = "an";
//        String s = "un";
//        String s = "der";
        int p = 100090;

        String dbUrl = "https://jsonmock.hackerrank.com/api/countries/search?name=" + s;
        String dbUrlSufix = "&page=";

        JSONObject jsonObject = getData(dbUrl);
        JSONArray data = (JSONArray) jsonObject.get("data");
        checkPopulation(data, p);

        Long total = (Long) jsonObject.get("total");
        Long perPage = (Long) jsonObject.get("per_page");
        Long pagesNum = (total / perPage) + 1;

        if (pagesNum > 1) {
            for (int x = 2; x <= pagesNum; x++) {
                JSONObject jsonObjectIter = getData(dbUrl + dbUrlSufix + x);
                JSONArray dataIter = (JSONArray) jsonObjectIter.get("data");
                checkPopulation(dataIter, p);
            }
        }

        System.out.println("resultCounter: " + resultCounter);

    }

    public static void checkPopulation(JSONArray data, int populationLimit) {
        for (Object element : data) {
            JSONObject object = (JSONObject) element;
            Long population = (Long) object.get("population");
            if (population >= populationLimit) {
                resultCounter++;
            }
        }
    }


    public static JSONObject getData(String url) throws Exception {
        InputStream is = new URL(url).openStream();
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            StringBuilder sb = new StringBuilder();
            int cp;
            while ((cp = rd.read()) != -1) {
                sb.append((char) cp);
            }

            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(sb.toString());

            return json;
        } finally {
            is.close();
        }
    }
}
