package local.sandbox.algorithms.flynerd.generator;

public class BubbleSort {
	public static void main(String[] args) {
		int[] array = null;
		try {
			array = Generator.generate(GenerationTypes.RANDOM, 50);
		} catch (GeneratorException e) {
			System.out.println(e.getMessage());
		}

		for (int x : array) {
			System.out.println(x);
		}
	}
}
