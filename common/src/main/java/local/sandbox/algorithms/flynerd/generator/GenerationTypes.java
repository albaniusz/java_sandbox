package local.sandbox.algorithms.flynerd.generator;

public enum GenerationTypes {
	RANDOM,
	ASC,
	DESC
}
