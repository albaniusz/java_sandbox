package local.sandbox.algorithms.flynerd.generator;

public class GeneratorException extends Exception {
	public GeneratorException(String message) {
		super(message);
	}
}
