package local.sandbox.algorithms.flynerd.generator;

import java.util.Random;

public class Generator {
	public static int[] generate(GenerationTypes type, int size) throws GeneratorException {
		int[] output = null;
		switch (type) {
			case RANDOM:
				output = random(size);
				break;
			case ASC:
				output = asc(size);
				break;
			case DESC:
				output = desc(size);
				break;
			default:
				throw new GeneratorException("Unknown list type");
		}

		return output;
	}

	private static int[] random(int size) {
		int[] array = new int[size];
		Random rand = new Random();

		for (int x = 0; x < size; x++) {
			array[x] = rand.nextInt(size);
		}

		return array;
	}

	private static int[] asc(int size) {
		int[] array = new int[size];

		for (int x = 0; x < size; x++) {
			array[x] = x + 1;
		}

		return array;
	}

	private static int[] desc(int size) {
		int[] array = new int[size];

		for (int x = size; x > 0; x--) {
			array[size - x] = x;
		}

		return array;
	}
}
