package local.sandbox.algorithms.sort;

import java.util.ArrayList;
import java.util.List;

/**
 * Algorytm ten jest najpopularniejszy, jeśli chodzi o ilość zastosowań, ponieważ w ogólnym
 * i uśrednionym przypadku jest on najbardziej optymalny, a jednocześnie prosty w implementacji.
 * Jego wadą jest negatywny przypadek, kiedy to złożoność wynosi n^2 .
 * <p>
 * Algorytm jest rekursywny, tj. odwołuje się do samego siebie i jest przykładem podejścia dziel
 * i zwyciężaj (divide and conquer). W każdym kroku ze zbioru elementów wybieramy dowolny element
 * (nazywany pivot; może to być po prostu pierwszy z brzegu lub losowy, dla nieuporządkowanych
 * danych nie ma to znaczenia) a następnie dzielimy pozostałe elementy na dwie grupy — te mniejsze
 * od wybranego elementu (‚przed nim’) oraz większe od niego (‚za nim’). Każdą z tych grup sortujemy
 * w ten sam sposób.
 * <p>
 * Algorytm ten jest stosunkowo prosty w implementacji, a jego wydajność jest zdecydowanie
 * wystarczająca w ogólnym przypadku. Uśredniona złożoność obliczeniowa to O(n*log n),
 * pamięciowa O(log n), ale w najgorszym przypadku złożoność obliczeniowa może wynieść O(n^2)
 * — jest to zależne zarówno od danych wejściowych jak i sposobu doboru punktu pivot.
 */
public class Quicksort {
	public static void main(String[] args) {
		int[] array = {1, 3, 4, 2, 5, 3, 1};
		List<Integer> integers = new ArrayList<>();
		for (Integer element : array) {
			integers.add(element);
		}

		System.out.println(integers);

		Quicksort quicksort = new Quicksort();
		integers = quicksort.quicksort(integers);

		System.out.println(integers);
	}

	public List<Integer> quicksort(List<Integer> kolekcja) {
		if (kolekcja.size() <= 1) {
			return kolekcja;
		}
		List<Integer> wynik = new ArrayList<Integer>();
		List<Integer> mniejsze = new ArrayList<Integer>();
		List<Integer> wieksze = new ArrayList<Integer>();
		Integer pivot = kolekcja.remove(kolekcja.size() / 2);
		for (Integer obiekt : kolekcja) {
			if (obiekt < pivot) {
				mniejsze.add(obiekt);
			} else {
				wieksze.add(obiekt);
			}
		}
		mniejsze = quicksort(mniejsze);
		wieksze = quicksort(wieksze);
		wynik.addAll(mniejsze);
		wynik.add(pivot);
		wynik.addAll(wieksze);

		return wynik;
	}
}
