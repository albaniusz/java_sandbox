package local.sandbox.algorithms.sort;

import local.sandbox.algorithms.helper.CollectionDisplay;

/**
 * Nazwa pochodzi od obrazowego przedstawienia zasady działania — w każdym ‚przebiegu’ kolejna wartość
 * w kolejności ‚wypływa’ jak bąbelek powietrza na powierzchnie wody.
 * <p>
 * W algorytmie tym w celu posortowania n elementów wykonujemy n-1 przebiegów. W każdym przebiegu
 * zaczynamy od początku zbioru, bierzemy dwa elementy i zamieniamy je miejscami tak, aby większy był‚
 * wyżej’ (lub z większym indeksem itp). W pierwszym kroku robimy to dla elementów 1 i 2, w drugim
 * kroku dla 2 i 3 itd. Każdy przebieg ‚wyciąga’ do góry kolejny element w kolejności, po wykonaniu
 * wszystkich przebiegów nasz zbiór jest posortowany.
 * <p>
 * Algorytm ten jest najmniej optymalny z tych podstawowych i bardzo nieefektywny obliczeniowo
 * — złożoność to O(n^2). Ma on jednak złożoność pamięciową O(1) (oznacza to, że poza miejscem
 * potrzebnym do przechowywania sortowanego zbioru, nie wymaga dodatkowej pamięci).
 * <p>
 * Zaletą jest trywialna implementacja i przewidywalna ilość operacji, identyczna dla przypadku
 * optymistycznego jak i pesymistycznego.
 */
public class BubbleSort {
	public static void main(String[] args) {
		BubbleSort bubbleSort = new BubbleSort();

		int[] collection = {1, 3, 4, 2, 5, 3, 1};
		CollectionDisplay.intArray(collection);

		bubbleSort.bubbleSort(collection);

		CollectionDisplay.intArray(collection);
	}

	public void bubbleSort(int[] kolekcja) {
		for (int i = 0; i < kolekcja.length; i++) {
			for (int j = 1; j < kolekcja.length; j++) {
				if (kolekcja[j] < kolekcja[j - 1]) {
					int wieksza = kolekcja[j - 1];
					kolekcja[j - 1] = kolekcja[j];
					kolekcja[j] = wieksza;
				}
			}
		}
	}
}
