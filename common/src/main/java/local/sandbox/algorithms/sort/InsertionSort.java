package local.sandbox.algorithms.sort;

import static local.sandbox.algorithms.AlgorythmTool.printArray;

public class InsertionSort {
	public static void main(String[] args) {
		int[] unsorted = new int[]{2, 1, 3};
		printArray(unsorted);

		InsertionSort insertionSort = new InsertionSort();
		int[] sorted = insertionSort.insertionSort(unsorted);

		printArray(sorted);
	}

	public int[] insertionSort(int[] array) {
		for (int i = 1; i < array.length; i++) {
			int key = array[i];
			int j = i - 1;
			while (j >= 0 && array[j] > key) {
				array[j + 1] = array[j];
				j = j - 1;
			}
			j++;
			array[j] = key;
		}

		return array;
	}
}
