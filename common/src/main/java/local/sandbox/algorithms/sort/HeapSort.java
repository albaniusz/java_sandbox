package local.sandbox.algorithms.sort;

import local.sandbox.algorithms.helper.CollectionDisplay;

/**
 * Sortowanie przez kopcowanie (częściej spotykaną nazwą, także w polskiej literaturze, jest heap sort) to kolejny
 * z algorytmów mający praktyczne zastosowanie — jego wydajność jest najczęściej minimalnie mniejsza niż QuickSort,
 * ale wydajność pesymistyczna jest zdecydowanie lepsza. Polega on na budowaniu kopca — czyli struktury, w której
 * elementy są posortowane w określonym porządku — a następnie iteracyjnym dodawaniu kolejnych elementów zbioru.
 * Po każdym dodaniu konieczne jest przywrócenie właściwości kopca, tzn. uporządkowanie elementów.
 * <p>
 * Kopiec jest strukturą podobną do drzewa — pod kątem złożoności wstawienie jednego elementu wymaga maksymalnie log(n)
 * operacji. Dlatego złożoność w pesymistycznym przypadku wynosi tyle samo, co w ogólnym przypadku i jest równa nlog n.
 * Z punktu widzenia tego algorytmu nie ma pesymistycznych przypadków, nie ma też optymistycznych przypadków — jego
 * złożoność jest przewidywalna co czyni go preferowanym w niektórych zastosowaniach, szczególnie przy obliczeniach
 * czasu rzeczywistego (np. w sterowaniu procesami produkcyjnymi, systemach krytycznych takich jak podsystemy
 * w samolotach itp). Algorytm ten ma złożoność pamięciową O(1), obliczeniową O(n log n).
 */
public class HeapSort {
	public static void main(String[] args) {
		int[] array = {1, 3, 4, 2, 5, 3, 1};

		CollectionDisplay.intArray(array);

		HeapSort heapSort = new HeapSort();
		heapSort.sort(array);

		CollectionDisplay.intArray(array);
	}

	public void sort(int arr[]) {
		buildHeap(arr);
		for (int i = arr.length - 1; i > 0; i--) {
			swap(arr, 0, i);
			buildMaxHeap(arr, 0, i - 1);
		}
	}

	/**
	 * Budujemy stertę
	 */
	public static void buildHeap(int arr[]) {
		for (int i = (arr.length - 1) / 2; i >= 0; i--) buildMaxHeap(arr, i, arr.length - 1);
	}

	/**
	 * Podmieniamy największy element w stercie
	 */
	public static void buildMaxHeap(int arr[], int i, int N) {
		int left = 2 * i;
		int right = 2 * i + 1;
		int max = i;
		if (left <= N && arr[left] > arr[i]) max = left;
		if (right <= N && arr[right] > arr[max]) max = right;
		if (max != i) {
			swap(arr, i, max);
			buildMaxHeap(arr, max, N);
		}
	}

	/**
	 * Zamieniamy dwa elementy w tablicy
	 */
	public static void swap(int arr[], int i, int j) {
		int tmp = arr[i];
		arr[i] = arr[j];
		arr[j] = tmp;
	}
}
