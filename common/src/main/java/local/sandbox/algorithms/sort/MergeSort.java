package local.sandbox.algorithms.sort;

import local.sandbox.algorithms.helper.CollectionDisplay;

import java.util.Arrays;

/**
 * Jest to kolejny przykład podejścia dziel i rządź — w tym wypadku polega on na podziale zbioru na n
 * równych podzbiorów (najczęściej jednoelementowych) a następnie scalaniu ich ze sobą uzyskując większe,
 * posortowane podzbiory. Najprostsza implementacja jest rekurencyjna, ale nie wynika to bezpośrednio
 * z samego algorytmu — scalanie może się odbywać iteracyjnie, dołączając kolejne podzbiory do jednego‚
 * posortowanego’. Dużą zaletą tego algorytmu jest możliwość zrównoleglenia — operacje sortowania można
 * wykonywać na kilku wątkach lub nawet na wielu różnych maszynach w tym samym czasie.
 */
public class MergeSort {
	public static void main(String[] args) {
		int[] collection = {1, 3, 4, 2, 5, 3, 1};

		CollectionDisplay.intArray(collection);

		MergeSort mergeSort = new MergeSort();
		mergeSort.sortowaniePrzezScalanie(collection);

		CollectionDisplay.intArray(collection);
	}

	public int[] scalDwie(int[] pierwsza, int[] druga) {
		int[] wynik = new int[pierwsza.length + druga.length];
		int indeksWynik = 0;
		int indeksPierwszy = 0, indeksDrugi = 0;
		while (indeksPierwszy < pierwsza.length || indeksDrugi < druga.length) {
			if ((indeksPierwszy < pierwsza.length && pierwsza[indeksPierwszy] < druga[indeksDrugi]) || indeksDrugi == druga.length) {
				wynik[indeksWynik] = pierwsza[indeksPierwszy];
				indeksPierwszy++;
			} else {
				wynik[indeksWynik] = druga[indeksDrugi];
				indeksDrugi++;
			}
			indeksWynik++;
		}
		return wynik;
	}

	public int[] sortowaniePrzezScalanie(int[] kolekcja) {
		int podzial = kolekcja.length / 2;
		int[] prawa = sortowaniePrzezScalanie(Arrays.copyOfRange(kolekcja, 0, podzial));
		int[] lewa = sortowaniePrzezScalanie(Arrays.copyOfRange(kolekcja, podzial, kolekcja.length));
		return scalDwie(prawa, lewa);
	}
}
