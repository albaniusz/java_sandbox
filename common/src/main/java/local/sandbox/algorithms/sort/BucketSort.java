package local.sandbox.algorithms.sort;

import local.sandbox.algorithms.helper.CollectionDisplay;

/**
 * Ten rodzaj sortowania jest najszybszy obliczeniowo, ale ma bardzo dużą złożoność pamięciową
 * rzędu O(m), gdzie m to ilość możliwych wartości, bliższe optymalnego jest O(n), gdzie n to liczba
 * różnych wartości wejścia. W wersji ‚ortodoksyjnej’ polega na stworzeniu mapy pomiędzy możliwymi
 * wartościami, a ilością ich wystąpień w zbiorze, po czym odtworzeniu zbioru na tej podstawie.
 * <p>
 * Ponieważ takie podejście jest możliwe właściwie tylko dla liczb i to w określonym zakresie,
 * często stosowane jest połączenie tego podejścia oraz innego algorytmu — sortowanie przez zliczanie
 * służy do pogrupowania wstępnego, a następnie w każdej grupie korzystając z innego algorytmu
 * wykonujemy właściwe sortowane. Przykładem może być sortowanie ciągów znaków — grupujemy je najpierw
 * wg pierwszej litery w 26 grup i każdą z nich sortujemy osobno, następnie scalając wyniki.
 * <p>
 * Przykładowa implementacja (wersja ‚dosłowna’ — z odtwarzaniem wartości; założenie: wejście to tylko
 * liczby z zakresu 0-ZAKRES_WARTOSCI_MAX):
 */
public class BucketSort {
	int ZAKRES_WARTOSCI_MAX = 1;

	public static void main(String[] args) {
		int[] array = {1, 3, 4, 2, 5, 3, 1};

		CollectionDisplay.intArray(array);

		BucketSort bucketSort = new BucketSort();
		bucketSort.przezZliczanie(array);

		CollectionDisplay.intArray(array);
	}

	public int[] przezZliczanie(int[] kolekcja) {
		int[] zliczenia = new int[ZAKRES_WARTOSCI_MAX];
		for (int obiekt : kolekcja) {
			zliczenia[obiekt]++;
		} //generujemy wynik
		int[] wynik = new int[kolekcja.length];
		for (int i = 0, j = 0; i < ZAKRES_WARTOSCI_MAX; i++) {
			while (zliczenia[i] > 0) {
				wynik[j] = i;
				j++;
			}
		}
		return wynik;
	}
}
