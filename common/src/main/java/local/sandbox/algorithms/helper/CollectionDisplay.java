package local.sandbox.algorithms.helper;

public class CollectionDisplay {
	public static void intArray(int[] collection) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append('{');
		boolean isFirst = true;
		for (Integer element : collection) {
			if (!isFirst) {
				stringBuilder.append(", ");
			} else {
				isFirst = false;
			}
			stringBuilder.append(element);
		}
		stringBuilder.append('}');

		System.out.println(stringBuilder.toString());
	}
}
