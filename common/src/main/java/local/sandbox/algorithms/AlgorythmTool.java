package local.sandbox.algorithms;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class AlgorythmTool {
	private static LocalDateTime timeStart;
	private static LocalDateTime timeEnd;

	public static void start() {
		noticeStart();
	}

	public static void finish() throws Exception {
		noticeEnd();
		System.out.println(getDifference());
	}

	public static void noticeStart() {
		timeStart = LocalDateTime.now();
	}

	public static void noticeEnd() {
		timeEnd = LocalDateTime.now();
	}

	public static String getDifference() throws Exception {
		if (timeStart == null || timeEnd == null) {
			throw new Exception("Time not set");
		}

		return "Finished in: " + timeStart.until(timeEnd, ChronoUnit.MILLIS) + "ms";
	}

	public static void printLine(String line) {
		System.out.println(line);
	}

	public static void printArray(int[] array) {
		for (int i : array) {
			System.out.print(i + ", ");
		}
		System.out.println();
	}
}
