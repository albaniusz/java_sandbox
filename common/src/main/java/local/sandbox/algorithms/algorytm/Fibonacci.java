package local.sandbox.algorithms.algorytm;

import local.sandbox.algorithms.AlgorythmTool;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Ciąg Finonacciego
 * www.algorytm.org
 * (c) 2005 Tomasz Lubiński
 */

public class Fibonacci {

	public static long fib(long n) {
		if ((n == 1) || (n == 2))
			return 1;
		else
			return fib(n - 1) + fib(n - 2);
	}

	public static void main(String[] args) throws Exception {
		int n;

		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Podaj, ktory wyraz ciagu Fibonacciego obliczyc");
		n = Integer.parseInt(bufferedReader.readLine());

		AlgorythmTool.start();
		System.out.println(n + "-ty wyraz ciagu Fibonacciego: " + fib(n));
		AlgorythmTool.finish();

		return;
	}
}
