package local.sandbox.algorithms.algorytm;

import local.sandbox.algorithms.AlgorythmTool;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class FibonacciArray {
	public static void main(String... args) throws Exception {
		AlgorythmTool.start();

		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		int fibonacciNumber = Integer.parseInt(reader.readLine());

		long[] result = new long[fibonacciNumber + 1];
		long last = 1L;
		long beforeLast = 0;

		for (int i = 0; i < fibonacciNumber + 1; i++) {
			long input;
			if (i == 0 || i == 1) {
				input = i;
			} else {
				input = last + beforeLast;
				beforeLast = last;
				last = input;
			}
			result[i] = input;
		}

		for (long element : result) {
			System.out.println(element);
		}

		AlgorythmTool.finish();
	}
}
