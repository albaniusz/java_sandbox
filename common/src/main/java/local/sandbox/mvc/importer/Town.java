package local.sandbox.mvc.importer;

/**
 * Town
 */
public class Town {

	public static final String pathToSourceFile = "./files/importer/towns.txt";
	public static final String pathToXmlFile = "./files/importer/towns.xml";

	public static void main(String[] args) {

		try {
			ToolImporter tool = new ToolImporter(pathToSourceFile, pathToXmlFile);
			tool.write("<towns>");

			String line;

			while ((line = tool.getBr().readLine()) != null) {
				tool.write("\t<item key=\"" + tool.prepareLabel(line) + "\">");
				tool.write(tool.prepareTag(tool.LANG_PL, line));
				tool.write(tool.prepareTag(tool.LANG_EN, line));
				tool.write("\t</item>");
			}

			tool.write("</towns>", false);
			tool.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
