package local.sandbox.mvc.importer;

/**
 * Country
 */
public class Country {

	public static final String pathToSourceFile = "./files/importer/countries.txt";
	public static final String pathToXmlFile = "./files/importer/countries.xml";

	public static void main(String[] args) {

		try {
			ToolImporter tool = new ToolImporter(pathToSourceFile, pathToXmlFile);
			tool.write("<countries>");

			String line;

			while ((line = tool.getBr().readLine()) != null) {
				String[] names = line.split("\t");

				tool.write("\t<item key=\"" + tool.prepareLabel(names[1]) + "\">");
				tool.write(tool.prepareTag(tool.LANG_PL, names[0]));
				tool.write(tool.prepareTag(tool.LANG_EN, names[1]));
				tool.write("\t</item>");
			}

			tool.write("</countries>", false);
			tool.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
