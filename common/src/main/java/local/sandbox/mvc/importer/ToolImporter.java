package local.sandbox.mvc.importer;

import java.io.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * ToolImporter
 */
public class ToolImporter {

	public static final int LANG_PL = 1;
	public static final int LANG_EN = 2;
	public static Set<String> tags = new HashSet<String>();

	BufferedReader br;
	BufferedWriter bw;

	public BufferedReader getBr() {
		return br;
	}

	public ToolImporter(String pathToSourceFile, String pathToXmlFile) throws IOException {

		br = new BufferedReader(new FileReader(pathToSourceFile));
		bw = new BufferedWriter(new FileWriter(pathToXmlFile));

		bw.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		bw.newLine();
	}

	public String removePolishChars(String string) {

		Map<Character, Character> map = new HashMap<Character, Character>();

//		map.put('ę', 'e');
//		map.put('ó', 'o');
//		map.put('ą', 'a');
//		map.put('ś', 's');
//		map.put('ł', 'l');
//		map.put('ż', 'z');
//		map.put('ź', 'z');
//		map.put('ć', 'c');
//		map.put('ń', 'n');
//		map.put('Ę', 'E');
//		map.put('Ó', 'O');
//		map.put('Ą', 'A');
//		map.put('Ś', 'S');
//		map.put('Ł', 'L');
//		map.put('Ż', 'Z');
//		map.put('Ź', 'Z');
//		map.put('Ć', 'C');
//		map.put('Ń', 'N');

		for (Character key : map.keySet()) {
			string = string.replace((char) key, (char) map.get(key));
		}

		return string;
	}

	public String prepareLabel(String string) throws Exception {

		string = removePolishChars(string);
		string = string.replaceAll("([_()/ ]+)", "_");
		string = string.toLowerCase();

		if (tags.contains(string)) {
			throw new Exception("Tag nie jest unikalny");
		}
		tags.add(string);

		return string;
	}

	public String prepareTag(int tag, String label) {

		String language = "pl";

		switch (tag) {
			case LANG_EN:
				label = removePolishChars(label);
				language = "en";
				break;
		}

		return "\t\t<label lang=\"" + language + "\">" + label + "</label>";
	}

	public void close() throws IOException {

		br.close();
		bw.close();
	}

	public void write(String line) throws IOException {
		write(line, true);
	}

	public void write(String line, boolean addEndline) throws IOException {

		bw.write(line);
		if (addEndline) {
			bw.newLine();
		}
	}
}
