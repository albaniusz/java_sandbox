package local.sandbox.mvc;

import local.sandbox.mvc.junkbox.SimpleListModel;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * TestList
 */
public class TestList {
    public static void main(String[] args) {
        String[] s = {"Obrazek1", "Obrazek2", "Obrazek3", "ObrazekN"};
        SimpleListModel lm = new SimpleListModel(s);
        JList list = new JList(lm);
    }
}
