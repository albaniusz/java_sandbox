package local.sandbox.mvc;

import javax.swing.*;

/**
 * TestAbstractDate
 */
public class TestAbstractDate {

    public static void main(String[] args) {


        AbstractListModel styczen = new AbstractListModel() {
            public Object getElementAt(int i) {
                return (i + 1) + " stycznia";
            }

            public int getSize() {
                return 31;
            }
        };
    }

}
