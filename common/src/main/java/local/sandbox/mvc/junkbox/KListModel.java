package local.sandbox.mvc.junkbox;

import javax.swing.*;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * KListModel
 */
public class KListModel extends AbstractListModel {

    static Calendar kalend = new GregorianCalendar();

    static String[] nazwaDnia = {"niedziela", "poniedziałek", "wtorek", "środa",
            "czwartek", "piątek", "sobota"};

    static String[] nazwaMies = {"stycznia", "lutego", "marca", "kwietnia",
            "maja", "czerwca", "lipca", "sierpnia", "września", "października",
            "listopada", "grudnia"
    };
    static int[] ldni = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    int rok;
    int mies;

    public KListModel(int rok, int mies) {
        this.rok = rok;
        this.mies = mies - 1;
    }

    public Object getElementAt(int i) {
        kalend.set(rok, mies, i + 1);
        int indDnia = kalend.get(Calendar.DAY_OF_WEEK) - 1;
        return (i + 1) + "  " + nazwaMies[mies] + " " + nazwaDnia[indDnia];
    }

    public int getSize() {
        return ldni[mies];
    }

}