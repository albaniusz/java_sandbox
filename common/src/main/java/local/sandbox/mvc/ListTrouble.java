package local.sandbox.mvc;

import java.util.ArrayList;
import java.util.List;

/**
 * ListTrouble
 */
public class ListTrouble {
	public static void main(String[] args) {

		List<Integer> list = new ArrayList<Integer>();

		list.add(1);
		list.add(2);
		list.add(3);
		list.add(4);
		list.add(5);

		for(Integer number : list) {
			if (number.equals(3)) {
				list.remove(number);//make error, use Iterator
			}
		}
	}
}
