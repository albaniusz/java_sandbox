package local.sandbox.mvc.exporter.parsemode;

/**
 * Trade
 */
public class Trade implements Tool {
	@Override
	public String generateXml(String string) {
		return "<ENGINE.BUSINESS_CATEGORIES name=\"" + string + "\" is_deleted=\"0\" />";
	}

	@Override
	public String generateProp(String key) {
		return "trade." + key;
	}
}
