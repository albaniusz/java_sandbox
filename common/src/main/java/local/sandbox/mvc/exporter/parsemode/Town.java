package local.sandbox.mvc.exporter.parsemode;

/**
 * Town
 */
public class Town implements Tool {
	@Override
	public String generateXml(String string) {
		return "<ENGINE.CITIES country_id=\"1\" name=\"" + string + "\" is_deleted=\"0\"/>";
	}

	@Override
	public String generateProp(String key) {
		return "town." + key;
	}
}
