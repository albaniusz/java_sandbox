package local.sandbox.mvc.exporter.parsemode;

/**
 * Tool
 */
public interface Tool {
	public String generateXml(String string);

	public String generateProp(String key);
}
