package local.sandbox.mvc.exporter.parsemode;

/**
 * Country
 */
public class Country implements Tool {
	@Override
	public String generateXml(String string) {
		return "<ENGINE.COUNTRIES name=\"" + string + "\" is_deleted=\"0\"/>";
	}

	@Override
	public String generateProp(String key) {
		return "country." + key;
	}
}
