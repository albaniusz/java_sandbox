package local.sandbox.mvc.exporter;

import local.sandbox.mvc.exporter.parsemode.Country;
import local.sandbox.mvc.exporter.parsemode.Tool;
import local.sandbox.mvc.exporter.parsemode.Town;
import local.sandbox.mvc.exporter.parsemode.Trade;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Properties;

/**
 * Exporter
 */
public class Exporter {

	public static final String XML_FILE_PATH = "./files/importer/countries.xml";

	public static final int PARSE_MODE_TOWN = 0;
	public static final int PARSE_MODE_COUNTRY = 1;
	public static final int PARSE_MODE_TRADE = 2;

	public static void main(String[] args) {

		try {

			int mode = PARSE_MODE_COUNTRY;
			Tool parser;
			switch (mode) {
				case PARSE_MODE_COUNTRY:
					parser = new Country();
					break;
				case PARSE_MODE_TOWN:
					parser = new Town();
					break;
				case PARSE_MODE_TRADE:
					parser = new Trade();
					break;
				default:
					throw new Exception("Nieobslugiwnay tryb");
			}

			DocumentBuilder dBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document doc = dBuilder.parse(new File(XML_FILE_PATH));
			doc.getDocumentElement().normalize();

			BufferedWriter bw = new BufferedWriter(new FileWriter("./files/importer/output.xml"));
			Properties propertiesPl = new Properties();
			Properties propertiesEn = new Properties();

			NodeList nList = doc.getElementsByTagName("item");
			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;

					String key = eElement.getAttribute("key");
					bw.write(parser.generateXml(key));
					bw.newLine();

					NodeList elements = eElement.getElementsByTagName("label");
					for (int i = 0; i < 2; i++) {

						if (((Element) elements.item(i)).getAttribute("lang").equals("pl")) {
							propertiesPl.setProperty(parser.generateProp(key), ((Element) elements.item(i)).getTextContent());
						} else {
							propertiesEn.setProperty(parser.generateProp(key), ((Element) elements.item(i)).getTextContent());
						}
					}
				}
			}

			propertiesPl.store(new FileWriter("./files/importer/outputprop_pl.prop"), null);
			propertiesEn.store(new FileWriter("./files/importer/outputprop_en.prop"), null);

			bw.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
