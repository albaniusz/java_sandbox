package local.sandbox.mvc;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import local.sandbox.mvc.junkbox.KListModel;

/**
 * List2
 */
public class List2 extends JFrame implements ActionListener {
    JList list;
    int rok;

    List2(int rok, int mies) {
        this.rok = rok;
        Container cp = getContentPane();
        list = new JList(new KListModel(rok, mies));
        cp.add(new JScrollPane(list), "Center");
        JPanel p = new JPanel(new GridLayout(2, 0));
        p.setBorder(BorderFactory.createTitledBorder("Miesiące"));
        for (int i = 1; i <= 12; i++) {
            JButton b = new JButton("" + i);
            b.addActionListener(this);
            p.add(b);
        }
        cp.add(p, "South");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        int mies = Integer.parseInt(e.getActionCommand());
        list.setModel(new KListModel(rok, mies));
    }

    public static void main(String[] args) {
        JTextField rok = new JTextField(10);
        JTextField mies = new JTextField(10);

        rok.setBorder(BorderFactory.createTitledBorder("Rok"));
        mies.setBorder(BorderFactory.createTitledBorder("Miesiąc"));
        JOptionPane.showMessageDialog(null, new JTextField[]{rok, mies});

        new List2(Integer.parseInt(rok.getText()), Integer.parseInt(mies.getText()));
    }
}
