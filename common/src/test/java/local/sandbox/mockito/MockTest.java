package local.sandbox.mockito;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

import static org.mockito.Mockito.when;

public class MockTest {
	@MockBean
	private BookRepository mockRepository;

	@Before
	public void init() {
		Book book = new Book(1L, "A Book");
		when(mockRepository.findById(1L)).thenReturn(Optional.of(book));
	}

	@Test
	public void miniTest() {
		mockRepository.findById(1L);
	}
}
