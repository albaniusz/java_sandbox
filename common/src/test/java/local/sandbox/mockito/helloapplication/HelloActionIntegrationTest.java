package local.sandbox.mockito.helloapplication;

import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;

public class HelloActionIntegrationTest {
	HelloActable helloAction;
	Greeter helloGreeter;
	Appendable helloWriterMock;

	@Before
	public void setUp() {
		helloGreeter = new HelloGreeter("welcome", " says ");
		helloWriterMock = mock(Appendable.class);
		helloAction = new HelloAction(helloGreeter, helloWriterMock);
	}

	@Test
	public void testSayHello() throws Exception {
		when(helloWriterMock.append(any(String.class))).thenReturn(helloWriterMock);

		helloAction.sayHello("integrationTest", "universe");

		verify(helloWriterMock, times(2)).append(any(String.class));
		verify(helloWriterMock, times(1)).append(eq("integrationTest says "));
		verify(helloWriterMock, times(1)).append(eq("welcome universe"));
	}
}
