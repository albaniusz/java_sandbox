package local.sandbox.patterns.observer;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

@Disabled
public class SomeObservableTest {

	@Test
	public void getState() {
		assertTrue(true);
	}

	@Test
	public void attach() {
		assertTrue(true);
	}

	@Test
	public void detach() {
		assertTrue(true);
	}

	@Test
	public void notifyObservers() {
		assertTrue(true);
	}
}
