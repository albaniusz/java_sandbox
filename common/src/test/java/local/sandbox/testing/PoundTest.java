package local.sandbox.testing;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PoundTest {
	@Test
	@SamouczekBefore
	void shouldConvert1Pound() {
		assertEquals(new BigDecimal("0.453592370000000022489672346637235023081302642822265625"),
				new Pound(BigDecimal.ONE).toKilograms());
	}
}
