package local.sandbox.testing;

import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

class RangeTest {
	@Test
	public void shouldSayThat15rIsInRange() {
		Range range = new Range(10, 20);
		assertTrue(range.isInRange(15));
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrownIllegalArgumentExceptionOnWrongParameters() {
		new Range(20, 10);
	}

	@Test
	public void shouldHaveProperErrorMessage() {
		try {
			new Range(20, 10);
			fail("Exception wasn't thrown!");
		} catch (IllegalArgumentException exception) {
			assertEquals("lowerBound is bigger than upperBound!", exception.getMessage());
		}
	}
}
