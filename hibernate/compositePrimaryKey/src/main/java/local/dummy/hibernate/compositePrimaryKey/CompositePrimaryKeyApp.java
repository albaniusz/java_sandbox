package local.dummy.hibernate.compositePrimaryKey;

import local.dummy.hibernate.compositePrimaryKey.entity.Article;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class CompositePrimaryKeyApp {
    public static void main(String[] args) {
        StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();

        Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();

        SessionFactory factory = meta.getSessionFactoryBuilder().build();
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();

        Article article  = new Article();

        article.setConfPathID(1);
        article.setLevelStation(2);

        article.setTitle("title!");
        article.setBody("lorem ipsum");
        article.setNumber(123);

        session.save(article);
        transaction.commit();

        System.out.println("successfully saved");

        factory.close();
        session.close();
    }
}
