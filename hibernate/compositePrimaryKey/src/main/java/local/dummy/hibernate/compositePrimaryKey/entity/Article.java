package local.dummy.hibernate.compositePrimaryKey.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import java.io.Serializable;

@Getter
@Setter
@Entity
@IdClass(ArticlePK.class)
public class Article implements Serializable {
    @Id
    private Integer levelStation;
    @Id
    private Integer confPathID;

    private String title;

    private String body;

    private Integer number;
}
