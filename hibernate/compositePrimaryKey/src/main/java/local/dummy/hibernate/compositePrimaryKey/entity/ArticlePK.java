package local.dummy.hibernate.compositePrimaryKey.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ArticlePK implements Serializable {
    protected Integer levelStation;

    protected Integer confPathID;
}
