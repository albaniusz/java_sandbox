package local.dummy.springbooty.common.criteriasubqeries;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Getter
@Setter
public class Author {
	@Id
	private Long id;
}
