package local.dummy.springbooty.common.criteriasubqeries;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
public class Book {
	@Id
	private Long id;

	@OneToMany
	private Set<Author> authors = new HashSet<>();
}
