package local.dummy.springbooty.common.criteriatut;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface BookRepository extends JpaRepository<Book, Long>, JpaSpecificationExecutor<Book> {

	static Specification<Book> hasAuthor(String author) {
		return (book, cq, cb) -> cb.equal(book.get("author"), author);
	}

	static Specification<Book> titleContains(String title) {
		return (book, cq, cb) -> cb.like(book.get("title"), "%" + title + "%");
	}
}
