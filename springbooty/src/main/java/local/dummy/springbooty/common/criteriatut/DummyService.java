package local.dummy.springbooty.common.criteriatut;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import static local.dummy.springbooty.common.criteriatut.BookRepository.hasAuthor;
import static local.dummy.springbooty.common.criteriatut.BookRepository.titleContains;
import static org.springframework.data.jpa.domain.Specification.where;

@Service
@AllArgsConstructor
public class DummyService {

	private final BookRepository bookRepository;

	public void dummy() {

		String author = "xxx";
		String title = "yyy";

		bookRepository.findAll(hasAuthor(author));
		bookRepository.findAll(where(hasAuthor(author)).and(titleContains(title)));
	}
}
