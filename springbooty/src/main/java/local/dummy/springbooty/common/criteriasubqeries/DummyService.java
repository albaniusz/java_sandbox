package local.dummy.springbooty.common.criteriasubqeries;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.List;

@Service
@AllArgsConstructor
public class DummyService {

	private final EntityManager em;

	public void dummy() {

		// create the outer query
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery cq = cb.createQuery(Author.class);
		Root root = cq.from(Author.class);

		// count books written by an author
		Subquery sub = cq.subquery(Long.class);
		Root subRoot = sub.from(Book.class);

//		SetJoin<Book, Author> subAuthors = subRoot.join(Book_.authors);
//		sub.select(cb.count(subRoot.get(Book_.id)));
//		sub.where(cb.equal(root.get(Author_.id), subAuthors.get(Author_.id)));

		// check the result of the subquery
		cq.where(cb.greaterThanOrEqualTo(sub, 3L));

		TypedQuery query = em.createQuery(cq);
		List authors = query.getResultList();
	}
}
