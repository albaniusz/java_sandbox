package local.dummy.dropwizard.dropwizardsandbox;

import io.dropwizard.Application;
import io.dropwizard.setup.Environment;

public class DropwizardSandboxApplication extends Application<DropwizardSandboxConfiguration> {
    public static void main(String[] args) throws Exception {
        new DropwizardSandboxApplication().run(args);
    }

    @Override
    public void run(DropwizardSandboxConfiguration dropwizardSandboxConfiguration, Environment environment) throws Exception {

    }
}
