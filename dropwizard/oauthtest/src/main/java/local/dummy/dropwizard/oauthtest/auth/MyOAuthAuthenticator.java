package local.dummy.dropwizard.oauthtest.auth;

import io.dropwizard.auth.AuthenticationException;
import io.dropwizard.auth.Authenticator;

import java.util.Optional;

public class MyOAuthAuthenticator implements Authenticator<String, User> {
    @Override
    public Optional<User> authenticate(String s) throws AuthenticationException {
        return Optional.empty();
    }
}
