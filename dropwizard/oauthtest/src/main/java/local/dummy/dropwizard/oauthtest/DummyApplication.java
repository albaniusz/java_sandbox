package local.dummy.dropwizard.oauthtest;

import io.dropwizard.Application;
import io.dropwizard.setup.Environment;

public class DummyApplication extends Application<DummyConfiguration> {
    public static void main(String[] args) throws Exception {
        new DummyApplication().run(args);
    }

    @Override
    public void run(DummyConfiguration dummyConfiguration, Environment environment) throws Exception {

    }
}
