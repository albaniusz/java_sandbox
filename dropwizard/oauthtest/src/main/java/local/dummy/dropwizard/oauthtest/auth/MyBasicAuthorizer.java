package local.dummy.dropwizard.oauthtest.auth;

import io.dropwizard.auth.Authorizer;

import javax.annotation.Nullable;
import javax.ws.rs.container.ContainerRequestContext;

public class MyBasicAuthorizer implements Authorizer<User> {
    @Override
    public boolean authorize(User user, String s) {
        return false;
    }

    @Override
    public boolean authorize(User principal, String role, @Nullable ContainerRequestContext requestContext) {
        return false;
    }
}
