package local.dummy.dropwizard.oauth;

import io.dropwizard.auth.AuthDynamicFeature;
import io.dropwizard.auth.AuthValueFactoryProvider;
import io.dropwizard.auth.oauth.OAuthCredentialAuthFilter;
import io.dropwizard.testing.junit5.DropwizardExtensionsSupport;
import io.dropwizard.testing.junit5.ResourceExtension;
import local.dummy.dropwizard.oauthtest.auth.MyAuthorizer;
import local.dummy.dropwizard.oauthtest.auth.MyOAuthAuthenticator;
import local.dummy.dropwizard.oauthtest.auth.ProtectedResource;
import local.dummy.dropwizard.oauthtest.auth.User;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;
import org.glassfish.jersey.test.grizzly.GrizzlyWebTestContainerFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(DropwizardExtensionsSupport.class)
public class OAuthResourceTest {
    public ResourceExtension resourceExtension = ResourceExtension
            .builder()
            .setTestContainerFactory(new GrizzlyWebTestContainerFactory())
            .addProvider(new AuthDynamicFeature(new OAuthCredentialAuthFilter.Builder<User>()
                    .setAuthenticator(new MyOAuthAuthenticator())
                    .setAuthorizer(new MyAuthorizer())
                    .setRealm("SUPER SECRET STUFF")
                    .setPrefix("Bearer")
                    .buildAuthFilter()))
            .addProvider(RolesAllowedDynamicFeature.class)
            .addProvider(new AuthValueFactoryProvider.Binder<>(User.class))
            .addResource(new ProtectedResource())
            .build();

    @Test
    public void testProtected() throws Exception {
        final Response response = resourceExtension.target("/protected")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .header("Authorization", "Bearer TOKEN")
                .get();

        assertEquals(200, response.getStatus());
    }
}
