package local.dummy.dropwizard.oauth;

import io.dropwizard.auth.AuthDynamicFeature;
import io.dropwizard.auth.AuthValueFactoryProvider;
import io.dropwizard.auth.basic.BasicCredentialAuthFilter;
import io.dropwizard.testing.junit5.DropwizardExtensionsSupport;
import io.dropwizard.testing.junit5.ResourceExtension;
import local.dummy.dropwizard.oauthtest.auth.MyBasicAuthenticator;
import local.dummy.dropwizard.oauthtest.auth.MyBasicAuthorizer;
import local.dummy.dropwizard.oauthtest.auth.ProtectedResource;
import local.dummy.dropwizard.oauthtest.auth.User;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;
import org.glassfish.jersey.test.grizzly.GrizzlyWebTestContainerFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import java.util.Base64;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(DropwizardExtensionsSupport.class)
public class BasicAuthResourceTest {
    public ResourceExtension resourceExtension = ResourceExtension
            .builder()
            .setTestContainerFactory(new GrizzlyWebTestContainerFactory())
            .addProvider(new AuthDynamicFeature(new BasicCredentialAuthFilter.Builder<User>()
                    .setAuthenticator(new MyBasicAuthenticator())
                    .setAuthorizer(new MyBasicAuthorizer())
                    .buildAuthFilter()))
            .addProvider(RolesAllowedDynamicFeature.class)
            .addProvider(new AuthValueFactoryProvider.Binder<>(User.class))
            .addResource(new ProtectedResource())
            .build();

    @Test
    public void testProtectedResource() {
        String credential = "Basic " + Base64.getEncoder().encodeToString("test@gmail.com:secret".getBytes());

        Response response = resourceExtension
                .target("/protected")
                .request()
                .header(HttpHeaders.AUTHORIZATION, credential)
                .get();

        assertEquals(200, response.getStatus());
    }
}
